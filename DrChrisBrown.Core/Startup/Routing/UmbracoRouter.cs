﻿using Umbraco.Web;
using System.Web.Routing;


namespace DrChrisBrown.Core
{
    public static class UmbracoRouter
    {
        public static void CustomRoute(string controllerName, string controllerMethod, string routeFormat)
        {
            RouteTable.Routes.MapUmbracoRoute(
				controllerMethod,
				routeFormat,
				new
				{
					controller = controllerName,
					action = controllerMethod
				},
				new CustomRouteHandler()
			);
        }
        
    }
}
