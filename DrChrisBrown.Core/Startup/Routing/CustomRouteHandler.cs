﻿using System.Web.Routing;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace DrChrisBrown.Core
{
    public class CustomRouteHandler : UmbracoVirtualNodeRouteHandler
    {
        protected override IPublishedContent FindContent(RequestContext requestContext, UmbracoContext umbracoContext)
        {
            var request = requestContext.HttpContext.Request;
            var path = request.Url.GetAbsolutePathDecoded();
            return UmbracoContext.Current.ContentCache.GetByRoute(path);
        }
    }
}
