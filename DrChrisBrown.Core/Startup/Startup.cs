﻿using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Core.Persistence;

namespace DrChrisBrown.Core.Startup
{
    public class Startup : IApplicationEventHandler
    {
        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            // Create custom tables if not exists
            CreateCustomTables(applicationContext);

            // Backoffice Article Events
            ContentService.Saving += BackOfficeEvents.ContentService_Saving;
            ContentService.Published += BackOfficeEvents.ContentService_Published;
            ContentService.UnPublished += BackOfficeEvents.ContentService_Unpublished;

            // re/create index on startup - remove once created in production
            ISearchEngineService searchEngine = new AzureSearchService();
            searchEngine.CreateIndexDefinition("aus" + "documents", Document.ToIndexDefinition());
        }

        public void CreateCustomTables(ApplicationContext appContext)
        {
            Database db;
            db = appContext.DatabaseContext.Database;
            var _dbHelper = new DatabaseSchemaHelper(db, appContext.ProfilingLogger.Logger, appContext.DatabaseContext.SqlSyntax);

            if (!_dbHelper.TableExist("IndexCache"))
            {
                // Create DB table - and set overwrite to false
                _dbHelper.CreateTable<IndexCache>(false);
            }
        }
    }

}
