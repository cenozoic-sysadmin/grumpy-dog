﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace DrChrisBrown.Core
{
    public static class BackOfficeEvents
    {
        public static void ContentService_Saving(IContentService contentService, SaveEventArgs<IContent> eventArgs)
        {
            var content = eventArgs.SavedEntities.First();
            foreach (var region in Region.All)
            {
                var fields = new UmbracoFields(region);
                if (content.HasProperty(fields.Body)) // Remove backoffice media picker's auto appending of image size params
                {
                    var body = (string)eventArgs.SavedEntities.First().GetValue(fields.Body) ?? "";
                    /* 
                     * (/media/[0-9a-zA-Z/\\-._=&;:\"]*)    # Group 1 - original image path
                     * ([\\?]{1}[0-9a-zA-Z/\\-._=&;:]*)     # Group 2 - query string to be discarded
                     */
                    Regex regex = new Regex("(/media/[0-9a-zA-Z/\\-@:;%_+.~#&=\"]*)([\\?]{1}[0-9a-zA-Z/\\-@:;%_+.~#&=]*)");
                    var matches = regex.Matches(body);
                    foreach (Match item in matches)
                    {
                        body = body.Replace(item.Groups[2].Value, "");
                    }
                    eventArgs.SavedEntities.First().SetValue(fields.Body, body);
                }
            }
        }

        public static void ContentService_Published(IPublishingStrategy publishingStrategy, PublishEventArgs<IContent> eventArgs)
        {
            var content = eventArgs.PublishedEntities.First();
            ISearchEngineService searchServices = new AzureSearchService();
            IIndexCacheService indexCacheService = new IndexCacheService(ApplicationContext.Current);
            var context = UmbracoContext.Current.UrlProvider;
            context.Mode = Umbraco.Web.Routing.UrlProviderMode.Relative;
            var urlName = context.GetUrl(content.Id).Split('/').Where(s => !string.IsNullOrEmpty(s)).LastOrDefault() ?? "";

            foreach (var region in Region.All)
            {
                var fields = new UmbracoFields(region);
                // if content exists in region
                if (content.HasProperty(fields.Section) && content.GetValue(fields.Section) != null)
                {
                    // Update Azure index
                    DateTimeOffset order; DateTimeOffset.TryParse(content.GetValue(fields.Order).ToString(), out order);
                    var helper = new UmbracoHelper(UmbracoContext.Current);
                    string sectionID = content.GetValue(fields.Section).ToString().Replace("umb://document/", "");
                    var sectionName = helper.TypedContent(new Guid(sectionID)).Name;

                    var doc = new Document()
                    {
                        docId = content.Id.ToString(),
                        order = order,
                        sections = sectionName,
                        tags = content.GetValue(fields.Tags).ToString().ToLower().Split(','),
                        title = content.GetValue(fields.Title).ToString(),
                        type = content.ContentType.Name,
                        urlName = urlName
                    };
                    var docs = new Document[1] { doc };
                    searchServices.CreateDocuments(region.Name.ToLower() + "documents", docs);
                    indexCacheService.AddOrUpdate(doc.ToIndexCache(region)); // update index cache
                }
            }
        }

        public static void ContentService_Unpublished(IPublishingStrategy publishingStrategy, PublishEventArgs<IContent> eventArgs)
        {
            ISearchEngineService searchServices = new AzureSearchService();
            IIndexCacheService indexCacheService = new IndexCacheService(ApplicationContext.Current);
            // Remove unpublished article's index on all regions
            var content = eventArgs.PublishedEntities.First();
            foreach (var region in Region.All)
            {
                var docs = new object[1] {
                    new {
                        docId = content.Id.ToString(),
                    }
                };
                searchServices.DeleteDocuments(region.Name.ToLower() + "documents", docs);
                indexCacheService.Delete(content.Id.ToString(), region.Name); // remove index cache
            }
        }

    }
}
