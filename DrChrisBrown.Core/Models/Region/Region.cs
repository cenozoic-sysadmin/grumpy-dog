﻿using System;

namespace DrChrisBrown.Core
{
    public sealed class Region
    {
        public readonly RegionCode Code;
        public readonly string Name;
        public readonly string Prefix;

        public static readonly Region Aus = new Region(RegionCode.AUS, "");
        public static readonly Region[] All = { Aus };

        private Region(RegionCode code, string prefix)
        {
            this.Code = code;
            this.Name = Enum.GetName(typeof(RegionCode), code);
            this.Prefix = prefix;
        }
    }

    public enum RegionCode
    {
        AUS = 0,
    }
}
