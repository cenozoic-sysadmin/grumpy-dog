﻿using System;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace DrChrisBrown.Core
{
    [TableName("IndexCache")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class IndexCache
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        public string DocId { get; set; }
        public string UrlName { get; set; }
        public DateTime Order { get; set; }
        [Length(4000)]
        public string Sections { get; set; }
        [Length(4000)]
        public string Tags { get; set; }
        [Length(4000)]
        public string Title { get; set; }
        public string Region { get; set; }
        public string Type { get; set; }
        public bool Rebuild { get; set; }

        public Document ToDocument()
        {
            // specify local time(AEST) to ensure Azure(UTC) timelines are in sync 
            DateTime newOrder = new DateTime(Order.Year, Order.Month, Order.Day, Order.Hour, Order.Minute, Order.Second, DateTimeKind.Local);
            return new Document() { docId = DocId, urlName = UrlName, order = newOrder, sections = Sections, tags = Tags.Split(','), title = Title, type = Type };
        }
    }
}