﻿declare @@articleContentType as int
declare @@videoContentType as int
declare @@galleryContentType as int

select @@articleContentType = nodeId from cmsContentType where alias = 'article'
select @@videoContentType = nodeId from cmsContentType where alias = 'video'
select @@galleryContentType = nodeId from cmsContentType where alias = 'gallery'

insert into IndexCache
select articles.DocId, articles.UrlName, articles.[Order], uN.text, articles.Tags, articles.Title, articles.Region, articles.Type, articles.Rebuild from (
	select
		isnull(cast(cCx.xml as xml).value('/article[1]/@id', 'nvarchar(max)'),'') as 'DocId',
		isnull(cast(cCx.xml as xml).value('/article[1]/@urlName', 'nvarchar(max)'),'') as 'UrlName',
		isnull(cast(cCx.xml as xml).value('/article[1]/order[1]', 'nvarchar(max)'),'') as 'Order',
		replace(isnull(cast(cCx.xml as xml).value('/article[1]/sections[1]', 'nvarchar(max)'), ''), 'umb://document/', '') as 'Sections',
		isnull(cast(cCx.xml as xml).value('/article[1]/tags[1]', 'nvarchar(max)'),'') as 'Tags',
		isnull(cast(cCx.xml as xml).value('/article[1]/title[1]', 'nvarchar(max)'),'') as 'Title',
		'aus' as 'Region',
		'Article' as 'Type',
		0 as 'Rebuild'
	from cmsContentXml cCX inner join cmsDocument cD 
	on cCX.nodeId = cD.nodeId and cD.published = 1 and cD.newest = 1
	inner join cmsContent cC on cC.nodeId = cCx.nodeId and cC.contentType = @@articleContentType
)
as articles
inner join umbracoNode uN
on len(articles.Sections) > 21 and uN.uniqueID = CAST(
SUBSTRING(articles.Sections, 1, 8) + '-' + SUBSTRING(articles.Sections, 9, 4) + '-' + SUBSTRING(articles.Sections, 13, 4) + '-' +
SUBSTRING(articles.Sections, 17, 4) + '-' + SUBSTRING(articles.Sections, 21, 12) AS UNIQUEIDENTIFIER)

union all

select videos.DocId, videos.UrlName, videos.[Order], uN.text, videos.Tags, videos.Title, videos.Region, videos.Type, videos.Rebuild from (
	select
		isnull(cast(cCx.xml as xml).value('/video[1]/@id', 'nvarchar(max)'),'') as 'DocId',
		isnull(cast(cCx.xml as xml).value('/video[1]/@urlName', 'nvarchar(max)'),'') as 'UrlName',
		isnull(cast(cCx.xml as xml).value('/video[1]/order[1]', 'nvarchar(max)'),'') as 'Order',
		replace(isnull(cast(cCx.xml as xml).value('/video[1]/sections[1]', 'nvarchar(max)'), ''), 'umb://document/', '') as 'Sections',
		isnull(cast(cCx.xml as xml).value('/video[1]/tags[1]', 'nvarchar(max)'),'') as 'Tags',
		isnull(cast(cCx.xml as xml).value('/video[1]/title[1]', 'nvarchar(max)'),'') as 'Title',
		'aus' as 'Region',
		'Video' as 'Type',
		0 as 'Rebuild'
	from cmsContentXml cCX inner join cmsDocument cD 
	on cCX.nodeId = cD.nodeId and cD.published = 1 and cD.newest = 1
	inner join cmsContent cC on cC.nodeId = cCx.nodeId and cC.contentType = @@videoContentType
)
as videos
inner join umbracoNode uN
on len(videos.Sections) > 21 and uN.uniqueID = CAST(
SUBSTRING(videos.Sections, 1, 8) + '-' + SUBSTRING(videos.Sections, 9, 4) + '-' + SUBSTRING(videos.Sections, 13, 4) + '-' +
SUBSTRING(videos.Sections, 17, 4) + '-' + SUBSTRING(videos.Sections, 21, 12) AS UNIQUEIDENTIFIER)

union all

select galleries.DocId, galleries.UrlName, galleries.[Order], uN.text, galleries.Tags, galleries.Title, galleries.Region, galleries.Type, galleries.Rebuild from (
	select
		isnull(cast(cCx.xml as xml).value('/gallery[1]/@id', 'nvarchar(max)'),'') as 'DocId',
		isnull(cast(cCx.xml as xml).value('/gallery[1]/@urlName', 'nvarchar(max)'),'') as 'UrlName',
		isnull(cast(cCx.xml as xml).value('/gallery[1]/order[1]', 'nvarchar(max)'),'') as 'Order',
		replace(isnull(cast(cCx.xml as xml).value('/gallery[1]/sections[1]', 'nvarchar(max)'), ''), 'umb://document/', '') as 'Sections',
		isnull(cast(cCx.xml as xml).value('/gallery[1]/tags[1]', 'nvarchar(max)'),'') as 'Tags',
		isnull(cast(cCx.xml as xml).value('/gallery[1]/title[1]', 'nvarchar(max)'),'') as 'Title',
		'aus' as 'Region',
		'Gallery' as 'Type',
		0 as 'Rebuild'
	from cmsContentXml cCX inner join cmsDocument cD 
	on cCX.nodeId = cD.nodeId and cD.published = 1 and cD.newest = 1
	inner join cmsContent cC on cC.nodeId = cCx.nodeId and cC.contentType = @@galleryContentType
)
as galleries
inner join umbracoNode uN
on len(galleries.Sections) > 21 and uN.uniqueID = CAST(
SUBSTRING(galleries.Sections, 1, 8) + '-' + SUBSTRING(galleries.Sections, 9, 4) + '-' + SUBSTRING(galleries.Sections, 13, 4) + '-' +
SUBSTRING(galleries.Sections, 17, 4) + '-' + SUBSTRING(galleries.Sections, 21, 12) AS UNIQUEIDENTIFIER)



