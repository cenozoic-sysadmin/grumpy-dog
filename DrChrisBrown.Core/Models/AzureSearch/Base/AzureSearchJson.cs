﻿using Newtonsoft.Json;

namespace DrChrisBrown.Core
{
    // These classes are to be serialized into json for Azure search api consumption

    // https://docs.microsoft.com/en-us/azure/search/search-create-index-dotnet
    public class AzureSearchIndex // for index creation
    {
        public string name { get; set; }
        public AzureSearchIndexFields[] fields { get; set; }

        public AzureSearchIndex(string indexName, AzureSearchIndexFields[] fields)
        {
            this.name = indexName;
            this.fields = fields;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    // https://docs.microsoft.com/en-us/azure/search/search-what-is-an-index
    public class AzureSearchIndexFields
    {
        public string name { get; set; }
        public string type { get; private set; }
        public bool searchable { get; set; }
        public bool filterable { get; set; }
        public bool sortable { get; set; }
        public bool facetable { get; set; }
        public bool key { get; set; }
        public bool retrievable { get; set; }
        public string analyzer { get; set; }

        public AzureSearchIndexFields(
            string fieldName, AzureSearchTypes dataType,
            bool searchable = false, 
            bool filterable = false, 
            bool sortable = false, 
            bool facetable = false, 
            bool iskey = false, 
            bool retrievable = true,
            AzureSearchAnalyzers analyser = null
        )
        {
            this.name = fieldName;
            this.type = dataType.name;
            this.searchable = searchable;
            this.filterable = filterable;
            this.sortable = sortable;
            this.facetable = facetable;
            this.key = iskey;
            this.retrievable = retrievable;
            this.analyzer = analyser == null ? "" : analyser.name;
        }
    }

    public sealed class AzureSearchTypes
    {
        public readonly string name;
        public static readonly AzureSearchTypes String = new AzureSearchTypes("Edm.String");
        public static readonly AzureSearchTypes StringCollection = new AzureSearchTypes("Collection(Edm.String)");
        public static readonly AzureSearchTypes Int32 = new AzureSearchTypes("Edm.Int32");
        public static readonly AzureSearchTypes Int64 = new AzureSearchTypes("Edm.Int64");
        public static readonly AzureSearchTypes Double = new AzureSearchTypes("Edm.Double");
        public static readonly AzureSearchTypes Boolean = new AzureSearchTypes("Edm.Boolean");
        public static readonly AzureSearchTypes DateTimeOffset = new AzureSearchTypes("Edm.DateTimeOffset");
        public static readonly AzureSearchTypes GeoPoint = new AzureSearchTypes("Edm.GeographyPoint");

        private AzureSearchTypes(string typeName)
        {
            this.name = typeName;
        }
    }

    public sealed class AzureSearchAnalyzers
    {
        public readonly string name;
        public static readonly AzureSearchAnalyzers EnglishLucene = new AzureSearchAnalyzers("en.lucene");

        private AzureSearchAnalyzers(string typeName)
        {
            this.name = typeName;
        }
    }


    // https://docs.microsoft.com/en-us/rest/api/searchservice/addupdate-or-delete-documents
    public class AzureSearchDocuments // for posting new, updating or deleting documents
    {
        public object[] value { get; set; }
        [JsonProperty("@search.action")]
        public string searchaction { get; set; }

        public AzureSearchDocuments(object[] documents, SearchAction action)
        {
            this.value = documents;
            this.searchaction = action.value;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    // https://docs.microsoft.com/en-us/rest/api/searchservice/search-documents
    // Only a subset of filters are been used here, add fields as required to expand api options
    public class AzureSearchQuery
    {
        public string search { get; set; }
        public string filter { get; set; }
        public bool count { get; set; }
        public string searchFields { get; set; }
        public string orderby { get; set; }
        public string top { get; set; }
        public string skip { get; set; }

        public AzureSearchQuery(bool count, int top, int skip, string term = "", string filter = "", string searchFields = "", string orderField = "", bool desc = true)
        {
            this.search = term;
            this.filter = filter;
            this.count = count;
            this.searchFields = searchFields;
            this.orderby = orderField + (desc ? " desc" : " asc");
            this.top = count ? "0" : top.ToString();
            this.skip = skip.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public abstract class AzureSearchResults
    {
        [JsonProperty("@odata.context")]
        public string context { get; set; }

        [JsonProperty("@odata.count")]
        public string count { get; set; }

        // object needs to be overridden with custom search result class in child class
        public virtual object[] value { get; set; }
    }

    public sealed class SearchAction
    {
        public readonly string value;

        public static readonly SearchAction Upload = new SearchAction("upload");
        public static readonly SearchAction Merge = new SearchAction("merge");
        public static readonly SearchAction MergeOrUpload = new SearchAction("mergeOrUpload");
        public static readonly SearchAction Delete = new SearchAction("delete");

        private SearchAction(string actionValue)
        {
            this.value = actionValue;
        }
    }
}
