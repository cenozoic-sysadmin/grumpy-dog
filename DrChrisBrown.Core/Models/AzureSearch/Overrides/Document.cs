﻿using System;

namespace DrChrisBrown.Core
{
    public class Document
    {
        public string docId { get; set; }
        public string urlName { get; set; }
        public DateTimeOffset? order { get; set; }
        public string sections { get; set; }
        public string[] tags { get; set; }
        public string title { get; set; }
        public string type { get; set; }

        public Document()
        {
            this.tags = new string[1] { "" };
        }

        public IndexCache ToIndexCache(Region region)
        {
            return new IndexCache() {
                DocId = docId,
                UrlName = urlName,
                Order = order.GetValueOrDefault().DateTime,
                Sections = sections,
                Tags = string.Join(",", tags),
                Title = title,
                Region = region.Name.ToLower(),
                Type = type
            };
        }

        public static AzureSearchIndexFields[] ToIndexDefinition()
        {
            Document doc = new Document();
            AzureSearchIndexFields[] fields = new AzureSearchIndexFields[7] {
                new AzureSearchIndexFields (nameof(doc.docId), AzureSearchTypes.String, true, true, false, true, true, true),
                new AzureSearchIndexFields (nameof(doc.urlName), AzureSearchTypes.String, true, true, false, true, false, true),
                new AzureSearchIndexFields (nameof(doc.order), AzureSearchTypes.DateTimeOffset, false, true, true, true, false, true),
                new AzureSearchIndexFields (nameof(doc.sections), AzureSearchTypes.String, true, true, false, true, false, true),
                new AzureSearchIndexFields (nameof(doc.tags), AzureSearchTypes.StringCollection, true, true, false, true, false, true),
                new AzureSearchIndexFields (nameof(doc.title), AzureSearchTypes.String, true, true, false, true, false, true, AzureSearchAnalyzers.EnglishLucene),
                new AzureSearchIndexFields (nameof(doc.type), AzureSearchTypes.String, true, true, false, true, false, true)
            };
            return fields;
        }
    }

}