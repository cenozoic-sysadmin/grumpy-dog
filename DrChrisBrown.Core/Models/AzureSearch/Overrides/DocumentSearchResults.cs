﻿namespace DrChrisBrown.Core
{
    public class DocumentSearchResults : AzureSearchResults
    {
        public Document[] value { get; set; }
    }
}
