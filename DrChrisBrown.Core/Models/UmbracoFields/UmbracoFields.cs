﻿namespace DrChrisBrown.Core
{
    // Abstracting magic strings for Article Field Aliases CRUD
    public class UmbracoFields
    {
        public string _prefix;

        public UmbracoFields(Region region = null)
        {
            this._prefix = region == null ? "" : region.Prefix;
        }

        public string Order => (_prefix + "order");
        public string Section => (_prefix + "sections");
        public string Tags => (_prefix + "tags");
        public string Title => (_prefix + "title");
        public string Body => (_prefix + "body");
    }
}
