﻿using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace DrChrisBrown.Core
{
    public class IndexController : SurfaceController
    {
        // /umbraco/surface/index/RebuildNext?param=xxxxx
        [HttpGet, IsBackOfficeUser]
        public JsonResult RebuildNext(string param)
        {
            IIndexCacheService indexCacheService = new IndexCacheService(ApplicationContext);
            var indexId = indexCacheService.RebuildNext();
            return this.SetResultsAsAllowGet(Json(
                new
                {
                    IndexId = indexId,
                    AzureError = indexId == -1,
                    BatchCompleted = indexId == 0,
                }
            ));
        }

        // /umbraco/surface/index/RebuildCount?param=xxxxx
        public JsonResult RebuildCount(string param)
        {
            IIndexCacheService indexCacheService = new IndexCacheService(ApplicationContext);
            var count = indexCacheService.RebuildCount();
            return this.SetResultsAsAllowGet(Json(
                new
                {
                    Count = count
                }
            ));
        }

    }
}
