﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace DrChrisBrown.Core
{
	public class DocumentController : Umbraco.Web.Mvc.SurfaceController
	{
        // Usage http://localhost:8619/umbraco/surface/document/articles?pageNumber=1&sections=dog&sections=cat
        [HttpGet] 
		public async Task<JsonResult> Articles(int? pageNumber, string[] sections)
		{
			ISearchService searchServices = new SearchService(Umbraco);
            List<string> sectionsList = sections == null ? new List<string>() : sections.ToList();
            return this.SetResultsAsAllowGet(Json(
                await searchServices.GetArticles(pageNumber ?? 1, 16, sectionsList)
			));
		}

        // Usage http://localhost:8619/umbraco/surface/document/videos?pageNumber=1
        [HttpGet]
        public async Task<JsonResult> Videos(int? pageNumber, string[] sections)
        {
            ISearchService searchServices = new SearchService(Umbraco);
            List<string> sectionsList = sections == null ? new List<string>() : sections.ToList();
            return this.SetResultsAsAllowGet(Json(
                await searchServices.GetVideos(pageNumber ?? 1, 16, sectionsList)
            ));
        }

        // Usage http://localhost:8619/umbraco/surface/document/galleries?pageNumber=1
        [HttpGet]
        public async Task<JsonResult> Galleries(int? pageNumber, string[] sections)
        {
            ISearchService searchServices = new SearchService(Umbraco);
            List<string> sectionsList = sections == null ? new List<string>() : sections.ToList();
            return this.SetResultsAsAllowGet(Json(
                await searchServices.GetGalleries(pageNumber ?? 1, 16, sectionsList)
            ));
        }

        // Usage http://localhost:8619/umbraco/surface/document/ArticlesAndVideos?pageNumber=1&sections=dog&sections=cat
        [HttpGet]
        public async Task<JsonResult> ArticlesAndVideos(int? pageNumber, string[] sections)
        {
            ISearchService searchServices = new SearchService(Umbraco);
            List<string> sectionsList = sections == null ? new List<string>() : sections.ToList();
            return this.SetResultsAsAllowGet(Json(
                await searchServices.GetArticlesAndVideos(pageNumber ?? 1, 16, sectionsList)
            ));
        }

        // Usage http://localhost:8619/umbraco/surface/document/article?name=test1-is-working
        [HttpGet]
        public async Task<JsonResult> Article(string name)
        {
            ISearchService searchServices = new SearchService(Umbraco);
            return this.SetResultsAsAllowGet(Json(
                await searchServices.GetArticle(name)
            ));
        }

        // Usage http://localhost:8619/umbraco/surface/document/video?name=test2-is-working
        [HttpGet]
        public async Task<JsonResult> Video(string name)
        {
            ISearchService searchServices = new SearchService(Umbraco);
            var res = searchServices.GetVideo(name);
            return this.SetResultsAsAllowGet(Json(
                await searchServices.GetVideo(name)
            ));
        }

        // Usage http://localhost:8619/umbraco/surface/document/gallery?name=test3-is-working
        [HttpGet]
        public async Task<JsonResult> Gallery(string name)
        {
            ISearchService searchServices = new SearchService(Umbraco);
            return this.SetResultsAsAllowGet(Json(
                await searchServices.GetGallery(name)
            ));
        }


    }
}
