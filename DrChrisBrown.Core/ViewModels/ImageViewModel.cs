﻿namespace DrChrisBrown.Core
{
    public class ImageViewModel
    {
        public string Url { get; set; }
        public string Orientation { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public ImageViewModel(string url, string width, string height)
        {
            this.Url = url;
            int widthInt = 0; int.TryParse(width, out widthInt);
            int heightInt = 0; int.TryParse(height, out heightInt);
            this.Width = widthInt;
            this.Height = heightInt;
            this.Orientation = widthInt > heightInt ? "landscape" : "portrait";
        }
    }
}
