﻿using System;

namespace DrChrisBrown.Core
{
	public class DocumentViewModel
	{
        public int ID { get; set; }
        public string Album { get; set; }
        public string Author { get; set; }
        public string Body { get; set; }
        public DateTime Date { get; set; }
        public string DisplayDate { get; set; }
        public ImageViewModel[] Images { get; set; }
        public string Location { get; set; }
        public string MetaDescription { get; set; }
        public string Photographer { get; set; }
        public string Tags { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public string VideoLink { get; set; }
	}
}
