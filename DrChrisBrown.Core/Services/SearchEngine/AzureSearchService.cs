﻿using Newtonsoft.Json;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DrChrisBrown.Core
{
    public class AzureSearchService : ISearchEngineService
    {
        #region Members

        private readonly HttpClient _restClient;
        private readonly string _apiKey;
        private readonly string _apiOrigin;
        private readonly string _apiVersion;

        #endregion

        public AzureSearchService()
        {
            this._restClient = new HttpClient(new HttpClientHandler());
            this._apiKey = ConfigurationManager.AppSettings["azure-search-api-key"];
            this._apiOrigin = "https://" + ConfigurationManager.AppSettings["azure-search-namespace"] + ".search.windows.net";
            this._apiVersion = "?api-version=" + ConfigurationManager.AppSettings["azure-search-api-verison"];
        }

        #region APIs

        // https://docs.microsoft.com/en-us/rest/api/searchservice/update-index
        public bool CreateIndexDefinition(string indexName, AzureSearchIndexFields[] fields)
        {
            var request = buildRequest(HttpMethod.Put, "/indexes/" + indexName);
            request.Content = buildBodyContent(new AzureSearchIndex(indexName, fields).ToJson());
            return _restClient.SendAsync(request).Result.IsSuccessStatusCode;
        }

        // https://docs.microsoft.com/en-us/rest/api/searchservice/addupdate-or-delete-documents
        public bool CreateDocuments(string indexName, object[] documents)
        {
            var request = buildRequest(HttpMethod.Post, "/indexes/" + indexName + "/docs/index");
            request.Content = buildBodyContent(new AzureSearchDocuments(documents, SearchAction.Upload).ToJson());
            return _restClient.SendAsync(request).Result.IsSuccessStatusCode;
        }

        public bool DeleteDocuments(string indexName, object[] documents) // Only ID (key field) is required
        {
            var request = buildRequest(HttpMethod.Post, "/indexes/" + indexName + "/docs/index");
            request.Content = buildBodyContent(new AzureSearchDocuments(documents, SearchAction.Delete).ToJson());
            return _restClient.SendAsync(request).Result.IsSuccessStatusCode;
        }

        // https://docs.microsoft.com/en-us/rest/api/searchservice/search-documents
        public T Search<T>(string indexName, AzureSearchQuery query) where T : AzureSearchResults
        {
            var request = buildRequest(HttpMethod.Post, "/indexes/" + indexName + "/docs/search");
            request.Content = buildBodyContent(query.ToJson());
            var responseBody = _restClient.SendAsync(request).Result.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<T>(responseBody);
        }

        public async Task<T> SearchAsync<T>(string indexName, AzureSearchQuery query) where T : AzureSearchResults
        {
            var request = buildRequest(HttpMethod.Post, "/indexes/" + indexName + "/docs/search");
            request.Content = buildBodyContent(query.ToJson());
            var response = await _restClient.SendAsync(request);
            var responseBody = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(responseBody);
        }

        #endregion

        #region Helpers

        private HttpRequestMessage buildRequest(HttpMethod method, string apiUrl)
        {
            var request = new HttpRequestMessage(method, this._apiOrigin + apiUrl + this._apiVersion);
            request.Headers.Add("api-key", this._apiKey);
            return request;
        }

        private StringContent buildBodyContent(string content)
        {
            return new StringContent(content, Encoding.UTF8, "application/json");
        }

        #endregion
    }
}
