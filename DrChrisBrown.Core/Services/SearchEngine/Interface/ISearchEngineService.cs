﻿using System.Threading.Tasks;

namespace DrChrisBrown.Core
{
    public interface ISearchEngineService
    {
        bool CreateIndexDefinition(string indexName, AzureSearchIndexFields[] fields);
        bool CreateDocuments(string indexName, object[] documents);
        bool DeleteDocuments(string indexName, object[] documents);
        T Search<T>(string indexName, AzureSearchQuery query) where T : AzureSearchResults;
        Task<T> SearchAsync<T>(string indexName, AzureSearchQuery query) where T : AzureSearchResults;
    }
}
