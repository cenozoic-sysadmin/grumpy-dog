﻿using Umbraco.Core;
using Umbraco.Core.Persistence;

namespace DrChrisBrown.Core
{
    public class IndexCacheService : IIndexCacheService
    {
        private readonly Database _db;

        public IndexCacheService(ApplicationContext appContext)
        {
            this._db = appContext.DatabaseContext.Database;
        }

        #region Public APIs

        public int RebuildNext()
        {
            var cache = _db.FirstOrDefault<IndexCache>("WHERE Rebuild = 1", "1");
            if (cache != null && cache.Id > 0)
            {
                var document = cache.ToDocument();
                ISearchEngineService azureSearch = new AzureSearchService();
                if (azureSearch.CreateDocuments(cache.Region + "documents", new Document[1] { document }))
                {
                    cache.Rebuild = false;
                    AddOrUpdate(cache);
                    return cache.Id;
                }
                else
                {
                    return -1; // Azure Search encountered an error creating the document
                }
            }
            return 0; // no more index to rebuild
        }

        public int RebuildCount()
        {
            return _db.ExecuteScalar<int>("SELECT Count(*) FROM IndexCache WHERE Rebuild = 1");
        }

        public void AddOrUpdate(IndexCache cache)
        {
            var existingCache = _db.FirstOrDefault<IndexCache>("WHERE DocId = (@0) and Region = (@1)", new string[2] { cache.DocId, cache.Region });
            if (existingCache == null)
            {
                _db.Insert("IndexCache", "Id", cache);
            }
            else
            {
                cache.Id = existingCache.Id;
                _db.Update(cache);
            }
        }

        public bool Delete(string articleId, string region)
        {
            var existingCache = _db.FirstOrDefault<IndexCache>("WHERE DocId = (@0) and Region = (@1)", new string[2] { articleId, region });
            if (existingCache != null)
            {
                _db.Delete(existingCache);
                return true;
            }
            return false;
        }

        #endregion

    }
}
