﻿namespace DrChrisBrown.Core
{
    public interface IIndexCacheService
    {
        int RebuildNext();
        int RebuildCount();
        void AddOrUpdate(IndexCache cache);
        bool Delete(string articleId, string region);
    }
}
