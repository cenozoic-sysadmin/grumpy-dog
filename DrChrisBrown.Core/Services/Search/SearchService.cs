﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace DrChrisBrown.Core
{
	public class SearchService : ISearchService
	{
		#region Members
		
		// For umbraco SurfaceController context
		private readonly UmbracoHelper _umbracoHelper;
        private readonly ISearchEngineService _searchEngine;
        private readonly Region _region;
		

		#endregion

		#region Ctor

		public SearchService(UmbracoHelper umbracoHelper)
		{
			this._umbracoHelper = umbracoHelper;
            this._searchEngine = new AzureSearchService();
            this._region = Region.Aus;
		}

		#endregion
		
		#region Methods

        public async Task<DocumentViewModel> GetArticle(string name)
        {
            var query = buildQuery(false, 1, 1, 0, "", "type eq 'Article' and urlName eq '" + name + "'", "", "order");
            return (await getDocumentsAsync(query)).FirstOrDefault();
        }

        public async Task<DocumentViewModel> GetVideo(string name)
        {
            var query = buildQuery(false, 1, 1, 0, "", "type eq 'Video' and urlName eq '" + name + "'", "", "order");
            return (await getDocumentsAsync(query)).FirstOrDefault();
        }

        public async Task<DocumentViewModel> GetGallery(string name)
        {
            var query = buildQuery(false, 1, 1, 0, "", "type eq 'Gallery' and urlName eq '" + name + "'", "", "order");
            return (await getDocumentsAsync(query)).FirstOrDefault();
        }

        public async Task<IEnumerable<DocumentViewModel>> GetArticles(int pageNumber, int pageSize, List<string> sectionNames)
		{
            var query = buildQuery(false, pageNumber, pageSize, 0, "", "type eq 'Article'" + getSectionsFilter(sectionNames), "", "order");
            return await getDocumentsAsync(query);
		}

        public async Task<IEnumerable<DocumentViewModel>> GetVideos(int pageNumber, int pageSize, List<string> sectionNames)
        {
            var query = buildQuery(false, pageNumber, pageSize, 0, "", "type eq 'Video'" + getSectionsFilter(sectionNames), "", "order");
            return await getDocumentsAsync(query);
        }

        public async Task<IEnumerable<DocumentViewModel>> GetGalleries(int pageNumber, int pageSize, List<string> sectionNames)
        {
            var query = buildQuery(false, pageNumber, pageSize, 0, "", "type eq 'Gallery'" + getSectionsFilter(sectionNames), "", "order");
            return await getDocumentsAsync(query);
        }

        public async Task<IEnumerable<DocumentViewModel>> GetArticlesAndVideos(int pageNumber, int pageSize, List<string> sectionNames)
        {
            var query = buildQuery(false, pageNumber, pageSize, 0, "", "(type eq 'Article' or type eq 'Video')" + getSectionsFilter(sectionNames), "", "order");
            return await getDocumentsAsync(query);
        }

        #endregion

        #region Helpers

        private AzureSearchQuery buildQuery(bool isCounting, int pageNumber, int pageSize, int offset, string term, string filter, string searchFields, string orderBy)
        {
            return new AzureSearchQuery(isCounting, pageSize, ((pageNumber - 1 < 0 ? 0 : pageNumber - 1) * pageSize) + offset, term, filter, searchFields, orderBy);
        }

        private async Task<IEnumerable<DocumentViewModel>> getDocumentsAsync(AzureSearchQuery query)
        {
            var results = await this._searchEngine.SearchAsync<DocumentSearchResults>(_region.Name.ToLower() + "documents", query);
            var transformedResults = (results == null || results.value == null) ? new List<DocumentViewModel>() : 
            results.value.Select(s => {
                var doc = _umbracoHelper.TypedContent(s.docId);
                var transform = transformToDocument(doc);
                return transform;
            });
            return transformedResults;
        }

        private DocumentViewModel transformToDocument(IPublishedContent content)
        {
            DocumentViewModel transform;
            var images = buildImagesArray(((dynamic)_umbracoHelper.TypedContent(content.Id)).HeroImage);
            DateTime orderDate = (DateTime) content.GetPropertyValue("order");
            transform = new DocumentViewModel
            {
                ID = content.Id,
                Author = (string) content.GetPropertyValue("author"),
                Body = (content.GetPropertyValue("body") ?? "").ToString(),
                Date = orderDate,
                DisplayDate = orderDate.ToString("ddd, dd MMM, yyyy"),
                Images = images,
                MetaDescription = (string) content.GetPropertyValue("metaDescription"),
                Tags = string.Join(",",((string[])content.GetPropertyValue("tags"))),
                Title = (string) content.GetPropertyValue("title"),
                Type = content.DocumentTypeAlias,
                Url = "/articles/" + content.UrlName
            };
            if (content.DocumentTypeAlias == "video")
            {
                transform.VideoLink = (string)content.GetPropertyValue("videoLink");
                transform.Url = "/videos/" + content.UrlName;
            }
            if (content.DocumentTypeAlias == "gallery")
            {
                transform.Album = (string) content.GetPropertyValue("album");
                transform.Location = (string) content.GetPropertyValue("location");
                transform.Photographer = (string)content.GetPropertyValue("author");
                transform.Url = "/galleries/" + content.UrlName;
            }
            return transform;
            
        }

        private ImageViewModel[] buildImagesArray(dynamic document)
        {
            List<IPublishedContent> heroImages = ((List<IPublishedContent>)document);
            return heroImages.Select(img => {
                return new ImageViewModel(img.Url, (string)img.GetPropertyValue("UmbracoWidth"), (string)img.GetPropertyValue("UmbracoHeight"));
            }).ToArray();
        }

        private string getSectionsFilter(List<string> sections)
        {
            if (sections == null || sections.Count == 0)
                return "";

            var filter = "";
            for (var i = 0; i < sections.Count; i++)
            {
                filter += (i == 0 ? " and (sections eq '" : " or sections eq '") + sections[i] + (i < sections.Count - 1 ? "'" : "')");
            }
            return filter;
        }

        #endregion
    }
}
