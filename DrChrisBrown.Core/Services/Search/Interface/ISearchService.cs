﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DrChrisBrown.Core
{
	public interface ISearchService
	{
		Task<IEnumerable<DocumentViewModel>> GetArticles(int pageNumber, int pageSize, List<string> sectionNames);
        Task<IEnumerable<DocumentViewModel>> GetVideos(int pageNumber, int pageSize, List<string> sectionNames);
        Task<IEnumerable<DocumentViewModel>> GetGalleries(int pageNumber, int pageSize, List<string> sectionNames);
        Task<IEnumerable<DocumentViewModel>> GetArticlesAndVideos(int pageNumber, int pageSize, List<string> sectionNames);
        Task<DocumentViewModel> GetArticle(string name);
        Task<DocumentViewModel> GetVideo(string name);
        Task<DocumentViewModel> GetGallery(string name);
    }
}
