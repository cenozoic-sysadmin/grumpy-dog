﻿using System;
using System.IO;
using System.Web.Mvc;
using Umbraco.Web.Mvc;


namespace DrChrisBrown.Core
{
	public static class ControllerExtensions
	{
		#region Extensions

		public static JsonResult SetResultsAsAllowGet(this SurfaceController c, JsonResult result)
		{
			result.ContentEncoding = System.Text.Encoding.UTF8;
			result.ContentType = "application/json";
			result.MaxJsonLength = Int32.MaxValue;
			result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
			result.RecursionLimit = 10;
			return result;
		}

		#endregion
	}
}
