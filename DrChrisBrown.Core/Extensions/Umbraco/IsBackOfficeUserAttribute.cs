﻿using System.Web.Mvc;

namespace DrChrisBrown.Core
{
    public class IsBackOfficeUserAttribute : ActionFilterAttribute
    {
        public string ParamName { get; set; }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var filter = new IsBackOfficeUserFilter(ParamName);
            filter.OnActionExecuting(context);
        }
    }
}
