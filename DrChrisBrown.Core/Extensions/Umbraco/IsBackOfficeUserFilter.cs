﻿using System.Web.Mvc;

namespace DrChrisBrown.Core
{
    public class IsBackOfficeUserFilter : IActionFilter
    {
        private string _param { get; set; }

        public IsBackOfficeUserFilter(string param)
        {
            this._param = param;
        }
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if ((string)filterContext.ActionParameters["param"] != "M90MW002DR")
                filterContext.Result = new EmptyResult();
        }
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {

        }
    }
}
