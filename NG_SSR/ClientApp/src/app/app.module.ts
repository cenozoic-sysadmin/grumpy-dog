import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule, getBaseUrl, getSingrUrl } from './app.routing';
import { isDesktop } from './app.device';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { JsonpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatDatepickerModule, MatNativeDateModule, MatAutocompleteModule, MatInputModule,
  MatSnackBarModule } from '@angular/material';

import { AppComponent } from './app.component';
import { BtnComponent } from './components/btn/btn.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { FileuploadComponent } from './components/fileupload/fileupload.component';
import { RadioComponent } from './components/radio/radio.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { FreetextComponent } from './components/freetext/freetext.component';
import { NumericComponent } from './components/numeric/numeric.component';
import { FullAddrComponent } from './components/full-addr/full-addr.component';
import { BreedComponent } from './components/breed/breed.component';
import { PetDetailsComponent } from './components/pet-details/pet-details.component';
import { EmailComponent } from './components/email/email.component';
import { TextareaComponent } from './components/textarea/textarea.component';
import { SnackBarComponent } from './components/snackbar/snackbar.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { SubscribeModalComponent } from './components/subscribe-modal/subscribe-modal.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { ContentListComponent } from './components/content-list/content-list.component';
import { ContentListDirective } from './components/content-list/content-list.directive';
import { VideoPlayerComponent } from './components/video-player/video-player.component';
import { HomeDirective } from './containers/home/home.directive';
import { ShareWidgetsComponent } from './components/sharewidgets/sharewidgets.component';
import { MailchimpSubscribeFormComponent } from './components/mailchimp-subscribe-form/mailchimp-subscribe-form.component';

import { ContactComponent } from './containers/contact/contact.component';
import { HomeComponent } from './containers/home/home.component';
import { PrivacyComponent } from './containers/privacy/privacy.component';
import { RegisterComponent } from './containers/register/register.component';
import { RegisterSuccessComponent } from './containers/register-success/register-success.component';
import { TermsComponent } from './containers/terms/terms.component';
import { ArticlesComponent } from './containers/articles/articles.component';
import { GalleriesComponent } from './containers/galleries/galleries.component';
import { VideosComponent } from './containers/videos/videos.component';
import { ArticleComponent } from './containers/article/article.component';
import { GalleryComponent } from './containers/gallery/gallery.component';
import { VideoComponent } from './containers/video/video.component';
import { JoinComponent } from './containers/join/join.component';

import { RootStoreModule } from './store';
import { ApiService } from './services/api.service';

@NgModule({
  declarations: [
    AppComponent,

    BtnComponent,
    DatepickerComponent,
    FileuploadComponent,
    RadioComponent,
    DropdownComponent,
    FreetextComponent,
    NumericComponent,
    FullAddrComponent,
    BreedComponent,
    PetDetailsComponent,
    EmailComponent,
    TextareaComponent,
    SnackBarComponent,
    NavMenuComponent,
    SubscribeModalComponent,
    CarouselComponent,
    ContentListComponent,
    ContentListDirective,
    VideoPlayerComponent,
    HomeDirective,
    ShareWidgetsComponent,
    MailchimpSubscribeFormComponent,

    ContactComponent,
    HomeComponent,
    PrivacyComponent,
    RegisterComponent,
    RegisterSuccessComponent,
    TermsComponent,
    ArticlesComponent,
    GalleriesComponent,
    VideosComponent,
    ArticleComponent,
    VideoComponent,
    GalleryComponent,
    JoinComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'app'}),
    AppRoutingModule,
    FormsModule,
    RootStoreModule,
    HttpClientModule,
    JsonpModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,

    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatInputModule,
    MatSnackBarModule
  ],
  providers: [
    { provide: 'BASE_URL', useFactory: getBaseUrl, deps: [] },
    { provide: 'SINGR_URL', useFactory: getSingrUrl, deps: [] },
    { provide: 'IS_DESKTOP', useFactory: isDesktop, deps: [] },
    HttpClient,
    ApiService,
    MatDatepickerModule,
    MatAutocompleteModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
