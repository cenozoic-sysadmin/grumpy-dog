import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { RootStoreState, RegisterActions, RegisterSelectors } from 'src/app/store';
import { SnackBarComponent } from 'src/app/components/snackbar/snackbar.component';
import { Referrals } from 'src/app/models/domain.model';
import { State } from 'src/app/store/store.state';
import { EmailComponent } from 'src/app/components/email/email.component';

@Component({
  selector: 'app-register-success',
  templateUrl: './register-success.component.html',
  styleUrls: ['./register-success.component.css']
})
export class RegisterSuccessComponent implements OnInit {
  @ViewChild('snackBar') snackBar: SnackBarComponent;
  @ViewChild('email1') email1: EmailComponent;
  @ViewChild('email2') email2: EmailComponent;
  @ViewChild('email3') email3: EmailComponent;
  referrals$: Observable<Referrals>;
  email1$: Observable<string>;
  email2$: Observable<string>;
  email3$: Observable<string>;
  isValid$: boolean;

  constructor(private store$: Store<RootStoreState.State>) {}

  ngOnInit() {
    this.referrals$ = this.store$.select(
      RegisterSelectors.selectReferrals
    );
  }

  setIsValid() {
    let state: State; this.store$.subscribe(s => state = s);
    this.isValid$ = RegisterSelectors.getIsReferrable(state.RegisterState);
  }

  setEmail1(email1: string) {
    this.referrals$.subscribe( r => r.Emails[0] = email1);
    this.setIsValid();
  }

  setEmail2(email2: string) {
    this.referrals$.subscribe( r => r.Emails[1] = email2);
    this.setIsValid();
  }

  setEmail3(email3: string) {
    this.referrals$.subscribe( r => r.Emails[2] = email3);
    this.setIsValid();
  }

  displayInvalidMessage() {
    this.snackBar.open('Please fill in at least one email address.', 3000, 'sb-alert-warning');
  }

  referToFriends() {
    this.store$.dispatch(new RegisterActions.ReferAction);
    this.email1.onKeyUp('');    this.email2.onKeyUp('');    this.email3.onKeyUp('');
    this.snackBar.open('Success! Your invitation has been sent.', 3000, 'sb-alert-success');
  }
}
