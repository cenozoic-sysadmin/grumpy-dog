import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootStoreState, ContentActions } from 'src/app/store';
import { Content } from 'src/app/models/domain.model';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.css']
})
export class JoinComponent implements OnInit, OnDestroy {

    content$: Observable<Content>;
    constructor(private store$: Store<RootStoreState.State>) {}

    ngOnInit() {
        const content = {
            ... new Content,
            Title: 'Join',
            MetaDescription: 'Join the Drool mailing list and receive regular wrap-ups of Dr. Chris Brown’s daily drips.',
            Images: [{
                Url: 'https://cms.drool.pet/media/assets/Drool_Meta_img_1280x720.jpg',
                Orientation: 'Landscape',
                Width: 1280,
                Height: 720
            }],
            Tags: 'join',
            Url: '/join'
        };
        this.store$.dispatch(new ContentActions.LoadMetaTagsAction(content));
        this.content$ = of(content);
    }

    ngOnDestroy() { }
}
