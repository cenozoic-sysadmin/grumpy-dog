import { Component, OnInit, AfterContentInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { RootStoreState, ContentActions, ContentSelectors } from 'src/app/store';
import { Content } from 'src/app/models/domain.model';

@Component({
    selector: 'app-video',
    templateUrl: './video.component.html',
    styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit, AfterContentInit, OnDestroy {
    loadAction: Action;
    appendAction: Action;
    contentList$: Observable<Content[]>;
    video$: Observable<Content>;
    contentSub: Subscription;
    scrollSub: Subscription;

    constructor(private route: ActivatedRoute, private store$: Store<RootStoreState.State>, private router: Router) {}

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.store$.dispatch(new ContentActions.LoadContentAction(params['name'], 'video'));
            this.video$ = this.store$.select(ContentSelectors.selectContent);
            this.contentSub = this.video$.subscribe(vid => {
                this.loadAction = new ContentActions.LoadContentListAction(vid, 'videos');
                this.appendAction = new ContentActions.AppendContentListAction(vid, 'videos');
                this.contentList$ = this.store$.select(ContentSelectors.selectContentList);
                this.store$.dispatch(this.loadAction);
            });
        });
    }

    ngAfterContentInit() {
        this.scrollSub = this.router.events.subscribe(() => {
            if (document !== undefined) {
                document.querySelectorAll('.content-container')[0].scrollTo(0, 0);
            }
        });
    }

    ngOnDestroy(): void {
        this.contentSub.unsubscribe();
        this.scrollSub.unsubscribe();
    }

}
