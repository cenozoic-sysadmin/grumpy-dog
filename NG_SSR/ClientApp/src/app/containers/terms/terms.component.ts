import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootStoreState, ContentActions } from 'src/app/store';
import { Content } from 'src/app/models/domain.model';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {
    constructor(private store$: Store<RootStoreState.State>) {}

    ngOnInit() {
        this.store$.dispatch(new ContentActions.LoadMetaTagsAction({
            ... new Content,
            Title: 'Terms',
            MetaDescription: 'These are the terms and conditions of use of our' +
            ' Platforms and apply whenever and however you access our Platforms.',
            Images: [{
                Url: 'https://cms.drool.pet/media/assets/Drool_Meta_img_1280x720.jpg',
                Orientation: 'Landscape',
                Width: 1280,
                Height: 720
            }],
            Tags: 'terms',
            Url: '/terms'
        }));
    }
}
