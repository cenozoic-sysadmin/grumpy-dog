import { Component, ViewChild, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { RootStoreState, ContactActions, ContactSelectors, ContentActions } from 'src/app/store';
import { Content } from 'src/app/models/domain.model';

import { State } from 'src/app/store/store.state';
import { SnackBarComponent } from 'src/app/components/snackbar/snackbar.component';
import { DropdownComponent } from 'src/app/components/dropdown/dropdown.component';
import { FreetextComponent } from 'src/app/components/freetext/freetext.component';
import { EmailComponent } from 'src/app/components/email/email.component';
import { TextareaComponent } from 'src/app/components/textarea/textarea.component';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  @ViewChild('firstName') firstName: FreetextComponent;
  @ViewChild('lastName') lastName: FreetextComponent;
  @ViewChild('email') email: EmailComponent;
  @ViewChild('reason') reason: DropdownComponent;
  @ViewChild('message') message: TextareaComponent;
  @ViewChild('snackBar') snackBar: SnackBarComponent;
  isValid$: boolean;

  constructor(private store$: Store<RootStoreState.State>) { }

    ngOnInit() {
        this.store$.dispatch(new ContentActions.LoadMetaTagsAction({
            ... new Content,
            Title: 'Contact Us',
            MetaDescription: 'If you need to contact Drool.pet contact us via this page.',
            Images: [{
                Url: 'https://cms.drool.pet/media/assets/Drool_Meta_img_1280x720.jpg',
                Orientation: 'Landscape',
                Width: 1280,
                Height: 720
            }],
            Tags: 'contact',
            Url: '/contact'
        }));
    }

  setFirstName(val: string) {
    this.store$.subscribe(s => s.ContactState.contact.FirstName = val);
    this.setIsValid();
  }

  setLastName(val: string) {
    this.store$.subscribe(s => s.ContactState.contact.LastName = val);
    this.setIsValid();
  }

  setEmail(val: string) {
    this.store$.subscribe(s => s.ContactState.contact.Email = val);
    this.setIsValid();
  }

  setReason(val: string) {
    this.store$.subscribe(s => s.ContactState.contact.Reason = val);
    this.setIsValid();
  }

  setMessage(val: string) {
    this.store$.subscribe(s => s.ContactState.contact.Message = val);
    this.setIsValid();
  }

  setIsValid() {
    let state: State; this.store$.subscribe( s => state = s);
    this.isValid$ = ContactSelectors.getIsValidated(state.ContactState);
  }

  submit() {
    this.store$.dispatch(new ContactActions.ContactAction({
      item: {
        FirstName: this.firstName.value,
        LastName: this.lastName.value,
        Email: this.email.value,
        Reason: this.reason.value,
        Message: this.message.value,
        Origin: environment.origin
      }
    }));
    this.firstName.onKeyUp(''); this.lastName.onKeyUp('');
    this.email.onKeyUp(''); this.message.onKeyUp('');
    this.reason.onSelected('Reason for contact (required)');
    this.snackBar.open('Success! You message has been sent. We will be in touch soon.', 3000, 'sb-alert-success');
  }

  displayInvalidMessage() {
    this.snackBar.open('Whoops! Looks like you missed a field.', 3000, 'sb-alert-warning');
  }

}
