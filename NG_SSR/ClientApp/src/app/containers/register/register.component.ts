import { Component, OnInit, AfterContentInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootStoreState, RegisterActions, RegisterSelectors, ContentActions } from 'src/app/store';
import { Observable } from 'rxjs';

import { Owner, Pet, GooglePlace, Content } from 'src/app/models/domain.model';
import { SnackBarComponent } from 'src/app/components/snackbar/snackbar.component';
import { State } from 'src/app/store/store.state';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, AfterContentInit {
  @ViewChild('snackBar') snackBar: SnackBarComponent;
  pets$: Observable<Pet[]>;
  owner$: Observable<Owner>;
  registered$: Observable<boolean>;
  isValid$: boolean;

  constructor(private store$: Store<RootStoreState.State>) { }

  ngOnInit() {
    this.pets$ = this.store$.select(
      RegisterSelectors.selectAllPets
    );
    this.owner$ = this.store$.select(
      RegisterSelectors.selectCurrentOwner
    );
    this.store$.dispatch(new ContentActions.LoadMetaTagsAction({
        ... new Content,
        Title: 'Register',
        MetaDescription: 'Register your pet with Dr. Chris Brown and receive regular wrap-ups of his daily drips.',
        Images: [{
            Url: 'https://cms.drool.pet/media/assets/Drool_Meta_img_1280x720.jpg',
            Orientation: 'Landscape',
            Width: 1280,
            Height: 720
        }],
        Tags: 'Register',
        Url: '/register'
    }));
  }

  ngAfterContentInit() {
    this.registered$ = this.store$.select(
      RegisterSelectors.selectIsRegistered
    );
  }

  setIsValid() {
    let state: State; this.store$.subscribe( s => state = s);
    this.isValid$ = RegisterSelectors.getIsValidated(state.RegisterState);
  }

  setFirstName(firstName: string) {
    this.owner$.subscribe(o => o.FirstName = firstName);
    this.setIsValid();
  }

  setLastName(lastName: string) {
    this.owner$.subscribe(o => o.LastName = lastName);
    this.setIsValid();
  }

  setGender(gender: number) {
    this.owner$.subscribe(o => o.Gender = gender);
    this.setIsValid();
  }

  setDoB(dob: Date) {
    this.owner$.subscribe(o => o.DoB = dob);
    this.setIsValid();
  }

  setMobile(mobile: number) {
    this.owner$.subscribe(o => o.Mobile = mobile);
    this.setIsValid();
  }

  setEmail(email: string) {
    this.owner$.subscribe(o => o.Email = email);
    this.setIsValid();
  }

  setAddress(addr: GooglePlace) {
    this.owner$.subscribe(o => o.Address = addr);
    this.setIsValid();
  }

  addAnotherPet() {
    this.store$.dispatch(new RegisterActions.AddPet);
    this.setIsValid();
  }

  displayInvalidMessage() {
    this.snackBar.open('Whoops! Looks like you missed the email field. Please enter your email to register.', 3000, 'sb-alert-warning');
  }

  submit() {
    this.store$.dispatch(new RegisterActions.RegisterAction);
  }
}
