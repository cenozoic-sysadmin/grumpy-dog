import { Directive, HostListener, EventEmitter, Output, OnInit, OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Directive({
    selector: '[appHomeDirective]'
})
export class HomeDirective implements OnInit, OnDestroy {
    @Output() homeResize: EventEmitter<any> = new EventEmitter();

    private delayResize = new Subject();
    private resizeSub: Subscription;

    constructor() {}

    ngOnInit() {
        this.resizeSub = this.delayResize.pipe(
            debounceTime(100)
        ).subscribe(e => this.homeResize.emit(e));
    }

    ngOnDestroy() {
        this.resizeSub.unsubscribe();
    }

    @HostListener('window:resize', ['$event'])
    resizeEvent(event) {
        event.preventDefault();
        event.stopPropagation();
        this.delayResize.next(event);
    }

}

