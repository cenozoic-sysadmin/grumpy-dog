import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { RootStoreState, ContentActions, ContentSelectors } from 'src/app/store';
import { Content } from 'src/app/models/domain.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    articleAndVideoList$: Observable<Content[]>;
    galleryList$: Observable<Content[]>;
    heroVideo$: Observable<Content>;
    videoClicked: boolean;

    constructor(private store$: Store<RootStoreState.State>, @Inject('IS_DESKTOP') public isDesktop: boolean) {}

    ngOnInit() {
        this.store$.dispatch(new ContentActions.LoadHomeArticleVideoListAction());
        this.articleAndVideoList$ = this.store$.select(ContentSelectors.selectHomePageArticleVideoList);
        this.store$.dispatch(new ContentActions.LoadHomeGalleryListAction);
        this.galleryList$ = this.store$.select(ContentSelectors.selectHomePageGalleryList);
        this.store$.dispatch(new ContentActions.LoadMetaTagsAction({
            ... new Content,
            Title: 'Home',
            MetaDescription: 'An online destination curated by Australian Veterinarian Dr. Chris Brown' +
            ' where you can find reliable, trustworthy information about your pets.',
            Images: [{
                Url: 'https://cms.drool.pet/media/assets/Drool_Meta_img_1280x720.jpg',
                Orientation: 'Landscape',
                Width: 1280,
                Height: 720
            }],
            Tags: 'home',
            Url: '/'
        }));
        if (this.isDesktop) {
            this.heroVideo$ = this.store$.select(ContentSelectors.selectHomePageHeroVideo);
        }
    }

    clicked() {
        if (this.isDesktop && !this.videoClicked) {
            this.videoClicked = true;
            if (document !== undefined) {
                document.getElementsByClassName('hero-copy')[0].classList.add('show');
                document.getElementsByClassName('scroll-icon')[0].classList.add('show');
                document.getElementsByClassName('hero-copy')[0].classList.remove('hide');
                document.getElementsByClassName('scroll-icon')[0].classList.remove('hide');
            }
        }
    }

}
