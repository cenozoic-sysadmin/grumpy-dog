import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootStoreState, ContentActions } from 'src/app/store';
import { Content } from 'src/app/models/domain.model';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {
    constructor(private store$: Store<RootStoreState.State>) {}

    ngOnInit() {
        this.store$.dispatch(new ContentActions.LoadMetaTagsAction({
            ... new Content,
            Title: 'Privacy',
            MetaDescription: 'This is our Privacy Policy and describes our practices in relation to information collected' +
            ' through all of our Drool services including advertising services, competitions and surveys,' +
            ' content sharing, websites, mobile sites, and applications.',
            Images: [{
                Url: 'https://cms.drool.pet/media/assets/Drool_Meta_img_1280x720.jpg',
                Orientation: 'Landscape',
                Width: 1280,
                Height: 720
            }],
            Tags: 'privacy',
            Url: '/privacy'
        }));
    }
}
