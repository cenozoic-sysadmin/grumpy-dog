import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Store, Action } from '@ngrx/store';
import { RootStoreState, ContentActions, ContentSelectors } from 'src/app/store';
import { Content } from 'src/app/models/domain.model';

@Component({
  selector: 'app-galleries',
  templateUrl: './galleries.component.html',
  styleUrls: ['./galleries.component.css']
})
export class GalleriesComponent implements OnInit {
    loadAction: Action;
    appendAction: Action;
    contentList$: Observable<Content[]>;

    constructor(private route: ActivatedRoute, private store$: Store<RootStoreState.State>) {}

    ngOnInit() {
        this.loadAction = new ContentActions.LoadContentListAction(new Content(), 'galleries');
        this.appendAction = new ContentActions.AppendContentListAction(new Content(), 'galleries');
        this.contentList$ = this.store$.select(ContentSelectors.selectContentList);
        this.route.queryParams.subscribe(params => {
            if (params['section'] !== undefined) {
                this.store$.dispatch(new ContentActions.SetSectionsAction(
                    params['section'].constructor === Array ? params['section'] : [params['section']],
                this.loadAction));
            } else {
                this.store$.dispatch(this.loadAction);
            }
        });
        this.store$.dispatch(new ContentActions.LoadMetaTagsAction({
            ... new Content,
            Title: 'Galleries',
            MetaDescription: 'Explore the world of animals with Dr. Chris Brown.',
            Images: [{
                Url: 'https://cms.drool.pet/media/assets/Drool_Meta_img_1280x720.jpg',
                Orientation: 'Landscape',
                Width: 1280,
                Height: 720
            }],
            Tags: 'galleries',
            Url: '/galleries'
        }));
    }
}
