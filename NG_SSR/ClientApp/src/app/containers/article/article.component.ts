import { Component, Inject, OnInit, AfterContentInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { RootStoreState, ContentActions, ContentSelectors } from 'src/app/store';
import { Content } from 'src/app/models/domain.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit, AfterContentInit, OnDestroy {
    loadAction: Action;
    appendAction: Action;
    contentList$: Observable<Content[]>;
    article$: Observable<Content>;
    contentSub: Subscription;
    scrollSub: Subscription;

    constructor(private route: ActivatedRoute, private store$: Store<RootStoreState.State>,
        private router: Router,
        @Inject('BASE_URL') public blobURL: string) {
        blobURL = blobURL.substring(0, blobURL.lastIndexOf('/'));
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.store$.dispatch(new ContentActions.LoadContentAction(params['name'], 'article'));
            this.article$ = this.store$.select(ContentSelectors.selectContent);
            this.contentSub = this.article$.subscribe(art => {
                this.loadAction = new ContentActions.LoadContentListAction(art, 'articles');
                this.appendAction = new ContentActions.AppendContentListAction(art, 'articles');
                this.contentList$ = this.store$.select(ContentSelectors.selectContentList);
                this.store$.dispatch(this.loadAction);
            });
        });
    }

    ngAfterContentInit(): void {
        this.scrollSub = this.router.events.subscribe(() => {
            if (document !== undefined) {
                document.querySelectorAll('.content-container')[0].scrollTo(0, 0);
            }
        });
    }

    ngOnDestroy(): void {
        this.contentSub.unsubscribe();
        this.scrollSub.unsubscribe();
    }
}
