import { Component, OnInit, AfterContentInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { Store, Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { RootStoreState, ContentActions, ContentSelectors } from 'src/app/store';
import { Content } from 'src/app/models/domain.model';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit, AfterContentInit, OnDestroy {
    loadAction: Action;
    appendAction: Action;
    contentList$: Observable<Content[]>;
    gallery$: Observable<Content>;
    contentSub: Subscription;
    scrollSub: Subscription;

    constructor(private route: ActivatedRoute, private store$: Store<RootStoreState.State>, private router: Router) {}

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.store$.dispatch(new ContentActions.LoadContentAction(params['name'], 'gallery'));
            this.gallery$ = this.store$.select(ContentSelectors.selectContent);
            this.contentSub = this.gallery$.subscribe(gal => {
                this.loadAction = new ContentActions.LoadContentListAction(gal, 'galleries');
                this.appendAction = new ContentActions.AppendContentListAction(gal, 'galleries');
                this.store$.dispatch(this.loadAction);
                this.contentList$ = this.store$.select(ContentSelectors.selectContentList);
            });
        });
    }

    ngAfterContentInit(): void {
        this.scrollSub = this.router.events.subscribe(() => {
            if (document !== undefined) {
                document.querySelectorAll('.content-container')[0].scrollTo(0, 0);
            }
        });
    }

    ngOnDestroy(): void {
        this.contentSub.unsubscribe();
        this.scrollSub.unsubscribe();
    }
}
