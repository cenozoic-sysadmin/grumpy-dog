import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { switchMap, debounceTime, startWith, filter, delay } from 'rxjs/operators';

import { ApiService } from 'src/app/services/api.service';
import { PetType } from 'src/app/models/domain.model';


@Component({
  selector: 'app-breed',
  templateUrl: './breed.component.html',
  styleUrls: ['./breed.component.css']
})

export class BreedComponent implements OnInit {
  @Input() placeholder: string;
  @Output() selected = new EventEmitter<string>();
  @ViewChild('matInput') matInput: ElementRef;

  searchForm: FormGroup;
  searchResult$: Observable<string[]>;
  petType: PetType;

  constructor(private fb: FormBuilder, private api: ApiService) {}

  ngOnInit() {
    this.searchForm = this.fb.group({
      searchInput: null
    });

    this.searchForm.get('searchInput').valueChanges
    .pipe(
      startWith(''),
      debounceTime(500),
      filter(value => value.length > 2),
      switchMap(value => this.api.getBreed(value, this.petType)),
    )
    .subscribe((results) => {
      this.searchResult$ = of(results);
    });
  }

  setPetType(petType: PetType) {
    if (this.petType !== undefined) {
      this.searchResult$ = of(new Array<string>());
    }
    this.petType = petType;
    this.matInput.nativeElement.value = '';
  }

  onSelected(breed: string) {
    this.selected.emit(breed);
  }

}
