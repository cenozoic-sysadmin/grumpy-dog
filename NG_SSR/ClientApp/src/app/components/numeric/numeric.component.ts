import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-numeric',
  templateUrl: './numeric.component.html',
  styleUrls: ['./numeric.component.css']
})

export class NumericComponent implements OnInit {
  @Input() placeholder: string;
  @Output() keyUp = new EventEmitter<number>();

  constructor() { }

  ngOnInit() { }

  onKeyUp(val: number) {
    this.keyUp.emit(val);
  }

}
