import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {

  isExpanded = false;
  @ViewChild('navbarToggler') togglerElem: ElementRef;

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  collapse() {
    if (this.isExpanded) {
      this.togglerElem.nativeElement.click();
    }
  }

}
