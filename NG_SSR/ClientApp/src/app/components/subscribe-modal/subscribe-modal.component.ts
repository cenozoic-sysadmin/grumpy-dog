import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { EmailComponent } from 'src/app/components/email/email.component';

@Component({
  selector: 'app-subscribe-modal',
  templateUrl: 'subscribe-modal.component.html',
  styles: ['./subscribe-modal.component.css'],
})

export class SubscribeModalComponent implements OnInit {
    @Input() modalID: string;
    @ViewChild('form') subForm: EmailComponent;
    @ViewChild('modal') modal: ElementRef;
    isValid$: boolean;

    constructor() {}

    ngOnInit() {}

    submitted(successful: boolean)  {
        this.modal.nativeElement.click(); // close modal
    }

}
