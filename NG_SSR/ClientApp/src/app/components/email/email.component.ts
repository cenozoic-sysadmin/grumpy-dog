import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})

export class EmailComponent implements OnInit {
  @Input() placeholder: string;
  @Output() keyUp = new EventEmitter<string>();
  @ViewChild('self') self: ElementRef;
  value: string;

  constructor() {}

  ngOnInit() {}

  onKeyUp(val: string) {
    this.keyUp.emit(val);
    this.value = val;
    this.self.nativeElement.value = val;
  }
}
