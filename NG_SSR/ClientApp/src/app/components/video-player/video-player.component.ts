/// <reference path="../../components/video-player/azuremediaplayer.d.ts" />
import { Component, Input, AfterViewInit, OnDestroy } from '@angular/core';
import { Content } from 'src/app/models/domain.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})

export class VideoPlayerComponent implements OnDestroy, AfterViewInit {
    @Input() videoID: string;
    @Input() showControls: boolean;
    @Input() autoplay: boolean;
    @Input() loops: boolean;
    @Input() posters: string;
    @Input() video$: Observable<Content>;
    player: any;
    options: any;

    constructor() {}

    ngAfterViewInit() {
        this.video$.subscribe(v => {
            if (v.Type === 'video' && v.VideoLink !== undefined) {
                this.init(v, this.showControls, this.autoplay, this.loops);
            }
        });
    }

    init(video: Content, showControls: boolean, autoplay: boolean, loops: boolean) {
        if (this.player === undefined) {
            this.options = {
                /* 'nativeControlsForTouch': false, */
                autoplay: autoplay,
                controls: showControls,
                loop: loops,
                plugins: {
                    ga: {
                        // 'eventLabel' : 'EventLabelForTracking', // default is URL source
                        'debug': false, // default is false
                        'eventsToTrack': [
                            'playTime', 'percentsPlayed', 'start', 'end', 'play', 'pause', 'fullscreen', 'seek', // user initiated events
                            'error'
                            // 'playerConfig', 'loaded', 'buffering', 'bitrate' // system initiated events
                        ],
                        // original had ['playerConfig', 'loaded', 'playTime', 'percentsPlayed', 'start', 'error', 'buffering', 'bitrate']
                        'percentsPlayedInterval': 20 // default is 20
                    }
                }
            };
            const player = amp(this.videoID, this.options);
            this.player = player;
            player.src([{
                'src': video.VideoLink,
                'type': 'application/vnd.ms-sstr+xml'
            }]);
            this.player.addEventListener(amp.eventName.waiting, startAutoPlay);
            this.player.addEventListener(amp.eventName.canplaythrough, startAutoPlay);
        } else if (this.player.cache_.src !== undefined && this.player.cache_.src.indexOf(video.VideoLink) === -1) {
            this.player.src([{
                'src': video.VideoLink,
                'type': 'application/vnd.ms-sstr+xml'
            }]);
            if (autoplay) {
                this.player.play();
            }
        }
        function startAutoPlay(e) {
            if (document !== undefined) {
                // doesn't work currently as Chrome and Safari disables autoplay by design
                if (autoplay && navigator.userAgent.match(/(Safari)|(Chrome)/i) !== null) {
                    const playBtn: HTMLElement = document.getElementsByClassName('vjs-big-play-button')[0] as HTMLElement;
                    playBtn.click();
                }
            }
        }
    }

    ngOnDestroy(): void {
        if (this.player !== undefined) {
            this.player.dispose();
        }
    }
}
