import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { switchMap, debounceTime, startWith } from 'rxjs/operators';

import { ApiService } from 'src/app/services/api.service';
import { GooglePlace } from 'src/app/models/domain.model';
import { setHostBindings } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-full-addr',
  templateUrl: './full-addr.component.html',
  styleUrls: ['./full-addr.component.css']
})

export class FullAddrComponent implements OnInit {
  @Input() placeholder: string;
  @Output() selected = new EventEmitter<GooglePlace>();

  searchForm: FormGroup;
  searchResult: GooglePlace[];
  selectedPlaceid: string;

  constructor(private fb: FormBuilder, private api: ApiService) {}

  ngOnInit() {

    this.searchForm = this.fb.group({
      searchInput: null
    });

    this.searchForm.get('searchInput').valueChanges
      .pipe(
        startWith(''),
        debounceTime(500),
        switchMap(value => this.api.getAddress(value))
      )
      .subscribe((results) => {
        this.searchResult = results;
      });
  }

  setPlaceId(place: GooglePlace) {
    this.selectedPlaceid = place.spatialId;
    this.selected.emit(place);
  }

}
