import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef} from '@angular/core';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.css']
})

export class TextareaComponent implements OnInit {
  @Input() placeholder: string;
  @Input() rows: number;
  @ViewChild('self') self: ElementRef;
  @Output() keyUp = new EventEmitter<string>();
  value: string;

  constructor() {}

  ngOnInit() {}

  onKeyUp(val: string) {
    this.keyUp.emit(val);
    this.value = val;
    this.self.nativeElement.value = val;
  }
}
