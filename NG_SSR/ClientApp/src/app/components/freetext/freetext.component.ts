import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-freetext',
  templateUrl: './freetext.component.html',
  styleUrls: ['./freetext.component.css']
})

export class FreetextComponent implements OnInit {
  @Input() placeholder: string;
  @Output() keyUp = new EventEmitter<string>();
  @ViewChild('self') self: ElementRef;
  value: string;

  constructor() { }

  ngOnInit() { }

  onKeyUp(val: string) {
    this.keyUp.emit(val);
    this.value = val;
    this.self.nativeElement.value = val;
  }

}
