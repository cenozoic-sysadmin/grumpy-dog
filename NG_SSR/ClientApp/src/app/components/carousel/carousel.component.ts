declare let ga: Function;
import { Component, Input, OnInit, Inject } from '@angular/core';
import { Observable, fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';
import { Content } from 'src/app/models/domain.model';
import { debounceTime } from 'rxjs/operators';


@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})

export class CarouselComponent implements OnInit {
    @Input() content: Content;
    @Input() galleryList$: Observable<Content[]>;
    @Input() carouselType: string;
    @Input() slideInterval: string;
    @Input() enableTracking: boolean;
    isMobile$:  Observable<boolean>;
    checkIfMobile = () => (window !== undefined && window.innerWidth < 992);


    constructor(@Inject('BASE_URL') public blobURL: string) {}

    ngOnInit() {
        this.isMobile$ = fromEvent(window, 'resize')
        .pipe(debounceTime(500))
        .pipe(map(() => window !== undefined && window.innerWidth < 994));
    }
}
