import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { SnackBarComponent } from 'src/app/components/snackbar/snackbar.component';
import { EmailComponent } from 'src/app/components/email/email.component';

@Component({
  selector: 'app-mailchimp-subscribe-form',
  templateUrl: 'mailchimp-subscribe-form.component.html',
  styles: ['./mailchimp-subscribe-form.component.css'],
})

export class MailchimpSubscribeFormComponent implements OnInit {
    @ViewChild('snackBar') snackBar: SnackBarComponent;
    @ViewChild('email') email: EmailComponent;
    isValid$: boolean;

    constructor(private api: ApiService) {}

    ngOnInit() {}

    validate(val: string) {
      this.isValid$ = val.length > 0;
    }

    displayInvalidMessage() {
      this.snackBar.open('Whoops! Looks like you missed the email field.', 3000, 'sb-alert-warning');
    }

    subscribe(): void {
      this.api.submitMailChimpMailingList('abfa3f96d231b8a1fd25efa84', '4e205b6bbf', this.email.value);
      this.email.onKeyUp(''); // clear email field
      this.snackBar.open('Success! Thank you for joining.', 3000, 'sb-alert-success');
    }

}
