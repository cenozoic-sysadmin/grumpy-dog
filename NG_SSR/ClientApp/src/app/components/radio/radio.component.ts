import { Component, Input, Output, OnInit, AfterViewInit, EventEmitter, ElementRef } from '@angular/core';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.css']
})

export class RadioComponent implements OnInit, AfterViewInit {
  @Input() selectables: string[];
  @Input() index: number;
  @Output() selected = new EventEmitter<number>();

  constructor(private element: ElementRef) {}

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.element.nativeElement.querySelector('input[id="' + this.selectables[0] + this.index.toString() + '"]').click();
  }

  onSelected(option: number) {
    this.selected.emit(option);
  }

}
