import { Directive, HostListener, EventEmitter, Output, OnInit, OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Directive({
    selector: '[appContentListDirective]'
})
export class ContentListDirective implements OnInit, OnDestroy {
    @Output() contentScroll: EventEmitter<any> = new EventEmitter();

    private scrolls = new Subject();
    private subscription: Subscription;

    constructor() {}

    ngOnInit() {
        this.subscription = this.scrolls.pipe(
            debounceTime(500)
        ).subscribe(e => this.contentScroll.emit(e));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    @HostListener('window:scroll', ['$event'])
    scrollEvent(event) {
        event.preventDefault();
        event.stopPropagation();
        this.scrolls.next(event);
    }
}

