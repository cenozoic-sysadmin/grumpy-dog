import { Component, OnInit, Input, Inject, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, Action } from '@ngrx/store';
import { RootStoreState, ContentActions, ContentSelectors } from 'src/app/store';
import { Content } from 'src/app/models/domain.model';
import { Location } from '@angular/common';

@Component({
  selector: 'app-content-list',
  templateUrl: './content-list.component.html',
  styleUrls: ['./content-list.component.css']
})

export class ContentListComponent implements OnInit, OnDestroy {
    @Input() listType: string;
    @Input() listTitle: string;
    @Input() loadAction: Action;
    @Input() appendAction: Action;
    @Input() contentList$: Observable<Content[]>;
    isLoading$: Observable<boolean>;

    containerClasses: string;
    dogSelected: boolean;
    catSelected: boolean;
    funSelected: boolean;
    constructor(private store$: Store<RootStoreState.State>, @Inject('BASE_URL') public blobURL: string, private location: Location) {
        blobURL = blobURL.substring(0, blobURL.lastIndexOf('/'));
        this.isLoading$ = this.store$.select(ContentSelectors.selectIsLoadingFirstPage);
    }

    ngOnInit() {
        this.containerClasses = 'container-fluid content-list ' + this.listType + '-list ';
        this.store$.select(ContentSelectors.selectSections).subscribe(sections => {
            this.dogSelected = (sections.indexOf('dog') > -1);
            this.catSelected = (sections.indexOf('cat') > -1);
            this.funSelected = (sections.indexOf('fun') > -1);
        });
    }

    injectContent(event: any) {
        // dispatch load event if element having infinity trigger class is visible
        if (document !== undefined) {
            if (this.elementInViewport(document.querySelectorAll('.infinity-trigger')[0])) {
                this.store$.dispatch(this.appendAction);
                this.contentList$ = this.store$.select(ContentSelectors.selectContentList);
            }
        }
    }

    toggleFilter(section: string) {
        if (document !== undefined) {
            this.store$.dispatch(new ContentActions.ToggleSectionAction(section, this.loadAction));
            let sections: string;
            this.store$.select(ContentSelectors.selectContentState).subscribe(cs => {
                sections = cs.sections.join('&section=');
            });
            this.location.go(document.location.pathname + (sections.length > 0 ? '?section=' + sections : ''));
        }
    }

    private elementInViewport(element) {
        if (document !== undefined) {
            if (element === undefined || element === null) {
                return false;
            }
            const rect = element.getBoundingClientRect();
            return (rect.bottom >= 0 && rect.right >= 0 && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
            && rect.left <= (window.innerWidth || document.documentElement.clientWidth));
        }
    }

    ngOnDestroy(): void {
        this.loadAction = null;
    }

}
