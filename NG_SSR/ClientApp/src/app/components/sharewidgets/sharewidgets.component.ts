import { Component, OnInit, Inject } from '@angular/core';
import { Meta } from '@angular/platform-browser';
declare var FB: any;

@Component({
  selector: 'app-sharewidgets',
  templateUrl: './sharewidgets.component.html',
  styleUrls: ['./sharewidgets.component.css']
})

export class ShareWidgetsComponent implements OnInit {
    constructor(private meta: Meta, @Inject('IS_DESKTOP') public isDesktop: boolean) {}

    ngOnInit() {}

    // https://developers.facebook.com/docs/javascript/reference/FB.ui/
    showFBDialog() {
        FB.ui({ method: 'share', href: this.ogURL() }, function(response) { });
    }

    // Copied URL from sharethis
    showFBMessengerDialog() {
        if (this.isDesktop) {
            this.showPopup('https://www.facebook.com/dialog/send?link='
            + encodeURIComponent(this.ogURL())
            + '&app_id=227122221438041&redirect_uri='
            + encodeURIComponent(this.ogURL()),
            'Facebook Messenger', 'height=553, width=826, top=100, left=150');
        } else {
            this.showPopup('fb-messenger://share/?link='
            + encodeURIComponent(this.ogURL())
            + '&app_id=227122221438041',
            'Facebook Messenger', 'height=553, width=826, top=100, left=150');
        }
    }

    // https://developer.twitter.com/en/docs/twitter-for-websites/tweet-button/guides/parameter-reference1.html
    showTwitterDialog() {
        this.showPopup('https://twitter.com/intent/tweet'
        + '?text=' + this.ogDesc()
        + '&url=' + this.ogURL()
        + '&hashtags=' + 'Drool,DrChrisBrown',
        'Twitter', 'height=553, width=826, top=100, left=150');
    }

    // https://developers.pinterest.com/tools/widget-builder/
    showPinterestDialog() {
        this.showPopup('https://www.pinterest.com.au/pin/create/button/'
        + '?description=' + this.ogDesc()
        + '&media=' + this.ogImg()
        + '&url=' + this.ogURL(),
        'Pinterest', 'height=720, width=800, top=100, left=150');
    }

    // https://css-tricks.com/simple-social-sharing-links/ - LinkedIn seems to be driven by og tags
    showLinkedInDialog() {
        this.showPopup('https://www.linkedin.com/shareArticle'
        + '?mini=true&url=' + this.ogURL()
        + '&title=' + this.ogTitle()
        + '&summary=' + this.ogDesc()
        + '&source=' + this.ogSiteName(),
        'LinkedIn', 'height=652, width=852, top=100, left=150');
    }

    showEmailDialog() {
        let a = document.createElement('a');
        a.href = 'mailto:'
        + '?subject=' + encodeURI(this.ogTitle())
        + '&body=' + encodeURI('Hi,\n\nI found this article from Drool.pet that you might be interested in:\n\n')
        + encodeURIComponent(this.ogURL()) + encodeURI('\n\n');
        a.target = '_top';
        a.click();
        a = null;
    }

    showSMSDialog() {
        this.showPopup('sms:?&body='
        + encodeURIComponent(this.ogURL()),
        'SMS', 'height=553, width=826, top=100, left=150');
    }

    private showPopup(url: string, name: string, dimensions: string) {
        if (window !== undefined) {
            window.open(url, name, dimensions);
        }
    }

    private ogDesc() { return this.meta.getTag('property="og:description"').content; }
    private ogTitle() { return this.meta.getTag('property="og:title"').content; }
    private ogImg() { return this.meta.getTag('property="og:image"').content; }
    private ogURL() { return this.meta.getTag('property="og:url"').content; }
    private ogSiteName() { return this.meta.getTag('property="og:site_name"').content; }
}
