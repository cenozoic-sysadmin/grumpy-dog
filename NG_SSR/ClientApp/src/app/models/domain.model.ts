import { environment } from 'src/environments/environment.prod';

export class Owner {
    Id: number;
    Pets: Pet[];
    FirstName: string;
    LastName: string;
    Gender: number;
    DoB: Date;
    Mobile: number;
    Email: string;
    Address: GooglePlace;
    Origin: string;

    constructor() {
        this.Pets = new Array<Pet>();
        this.Pets.push(new Pet());
        this.DoB = new Date();
        this.Address = new GooglePlace();
        this.Origin = environment.origin;
    }
}

export class Pet {
    Id: number;
    Type: number;
    Name: string;
    Breed: string;
    BloodType: number;
    Size: number;
    ActivityLevel: number;
    Weight: number;
    Gender: number;
    Age: number;
    AgeUnit: number;
    Photo: File;
}

export class Contact {
    FirstName: string;
    LastName: string;
    Email: string;
    Reason: string;
    Message: string;
    Origin: string;
    constructor() {
        this.Origin = environment.origin;
    }
}

export class Referrals {
    OwnerToken: string;
    Emails: string[];
    Origin: string;

    constructor() {
        this.Emails = new Array<string>();
        this.Origin = environment.origin;
    }
}

export class GooglePlace {
    spatialId: string;
    fullAddress: string;
}

export enum PetType {
    dog = 0,
    cat = 1
}

export class Content {
    ID: number;
    Album: string;
    Author: string;
    Body: string;
    Date: Date;
    DisplayDate: string;
    Images: Image[];
    Location: string;
    MetaDescription: string;
    Tags: string;
    Title: string;
    Type: string;
    Url: string;
    VideoLink: string;
    IsInfinityTrigger: boolean;
}

export class Image {
    Url: string;
    Orientation: string;
    Width: number;
    Height: number;
}
