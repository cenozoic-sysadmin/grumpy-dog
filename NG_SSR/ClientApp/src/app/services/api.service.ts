import { Injectable, Inject } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Jsonp } from '@angular/http';
import { Owner, GooglePlace, Contact, PetType, Referrals, Content } from '../models/domain.model';
import { environment } from 'src/environments/environment.prod';

@Injectable()
export class ApiService {
    constructor(
        private http: HttpClient,
        private jsonp: Jsonp,
        private meta: Meta,
        private title: Title,
        @Inject(DOCUMENT) private dom,
        @Inject('BASE_URL') private baseURL: string,
        @Inject('SINGR_URL') private singrURL: string
    ) {}

    registerOwner(ownerDetails: Owner): Observable<string> {
        return this.http.post<string>(
            `${this.singrURL}/api/Register/Register`, ownerDetails
        ).pipe(map(result => result));
    }

    uploadPetPhotos(ownerDetails: Owner, ownerGUID: string): Observable<boolean> {
        const formData: FormData = new FormData();
        formData.append('photosDTO.OwnerGUID', ownerGUID);
        let indexcount = 0;
        for (let i = 0; i < ownerDetails.Pets.length; i++) {
            if (ownerDetails.Pets[i].Photo != null) {
                formData.append('photosDTO.PetPhotosIndex[' + indexcount + ']', i.toString());
                indexcount++;
            }
            formData.append('photosDTO.PetPhotos', ownerDetails.Pets[i].Photo);
        }
        return this.http.post<boolean>(
            `${this.singrURL}/api/Register/UploadPetPhotos`, formData
        ).pipe(map(result => result));
    }

    referToFriends(referrals: Referrals): Observable<boolean> {
        return this.http.post<boolean>(
            `${this.singrURL}/api/Register/ReferToFriends`, referrals
        ).pipe(map(result => result));
    }

    submitContactUs(contactDetails: Contact): Observable<boolean> {
        return this.http.post<boolean>(
            `${this.singrURL}/api/Contact/SubmitContactUs`, contactDetails
        ).pipe(map(result => result));
    }

    getAddress(word: string): Observable<GooglePlace[]> {
        return this.http.get<GooglePlace[]>(
            `${this.singrURL}/api/Address/Autocomplete?input=` + word,
        ).pipe(map(result => result));
    }

    getBreed(word: string, petType: PetType): Observable<string[]> {
        return this.http.get<string[]>(
            `${this.singrURL}/api/Pet/GetBreeds?input=` + word + `&petType=` + petType,
        ).pipe(map(result => result));
    }

    submitMailChimpMailingList(apiKey: string, mailingListId: string, email: string): boolean {
        this.jsonp.request(
            `https://pet.us20.list-manage.com/subscribe/post?u=` + apiKey + `&id=` + mailingListId +
            `&subscribe=Subscribe&EMAIL=` + email + `&c=JSONP_CALLBACK`,
            { method: 'Get'}
        ).subscribe(response => {
            console.log(response);
        });
        return true;
    }

    getContent(name: string, api: string) {
        return this.http.get<Content>(
            `${this.baseURL}/umbraco/surface/document/` + api + `?name=` + name
        ).pipe(map((results) => {
            this.populateMetaTags(results);
            return results;
        }));
    }

    getContentList(pageNumber: number, sections: string[], api: string): Observable<Content[]> {
        const sectionsString = sections === undefined || sections.length === 0 ? '' : '&sections=' + sections.join(`&sections=`);
        return this.http.get<Content[]>(
            `${this.baseURL}/umbraco/surface/document/` + api + `?pageNumber=` + pageNumber.toString() + sectionsString
        ).pipe(map(results => results));
    }

    populateMetaTags(content: Content) {
        const title = isNullOrUndefined(content.Title) ? 'Drool By Dr. Chris Brown' : content.Title + ' - Drool By Dr. Chris Brown';
        const author = isNullOrUndefined(content.Author) ? 'Dr Chris Brown' : content.Author;
        const metaDesc = isNullOrUndefined(content.MetaDescription) ? '' : content.MetaDescription;
        const tags = isNullOrUndefined(content.Tags) ? '' : content.Tags;
        const link: string = environment.origin + content.Url;
        let heroImageUrl = '';
        let imageWidth = 0;
        let imageHeight = 0;
        if (!isNullOrUndefined(content.Images) && content.Images.length > 0) {
            heroImageUrl = content.Images[0].Url.indexOf('http') === -1 ?
            this.baseURL + content.Images[0].Url : content.Images[0].Url;
            imageWidth = content.Images[0].Width;
            imageHeight = content.Images[0].Height;
        }

        this.title.setTitle(title);
        this.meta.updateTag({ name: 'description', content: metaDesc });
        this.meta.updateTag({ name: 'author', content: author });
        this.meta.updateTag({ name: 'keywords', content: tags });
        const linkElem: HTMLLinkElement = this.dom.querySelector('link[rel="canonical"]');
        linkElem.href = link;

        this.meta.updateTag({ property: 'og:title', content: title });
        this.meta.updateTag({ property: 'og:description', content: metaDesc });
        this.meta.updateTag({ property: 'og:image', content: heroImageUrl });
        this.meta.updateTag({ property: 'og:image:width', content: imageWidth.toString() });
        this.meta.updateTag({ property: 'og:image:height', content: imageHeight.toString() });
        this.meta.updateTag({ property: 'og:url', content: link });

        this.meta.updateTag({ name: 'twitter:url', content: link });
        this.meta.updateTag({ name: 'twitter:title', content: title });
        this.meta.updateTag({ name: 'twitter:text:title', content: title });
        this.meta.updateTag({ name: 'twitter:description', content: metaDesc });
        this.meta.updateTag({ name: 'twitter:image', content: heroImageUrl });
        this.meta.updateTag({ name: 'tw-line', content: metaDesc });

        function isNullOrUndefined(any: any) {
            return any === undefined || any === null;
        }
    }

}
