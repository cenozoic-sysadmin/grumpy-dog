import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ContactComponent } from './containers/contact/contact.component';
import { HomeComponent } from './containers/home/home.component';
import { PrivacyComponent } from './containers/privacy/privacy.component';
import { RegisterComponent } from './containers/register/register.component';
import { TermsComponent } from './containers/terms/terms.component';
import { ArticlesComponent } from './containers/articles/articles.component';
import { GalleriesComponent } from './containers/galleries/galleries.component';
import { VideosComponent } from './containers/videos/videos.component';
import { ArticleComponent } from './containers/article/article.component';
import { VideoComponent } from './containers/video/video.component';
import { GalleryComponent } from './containers/gallery/gallery.component';
import { environment } from 'src/environments/environment.prod';
import { JoinComponent } from './containers/join/join.component';

export function getBaseUrl() {
    return environment.cmsURL;
}

export function getSingrUrl() {
    return environment.singrURL;
}

const routes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'contact', component: ContactComponent },
    { path: 'privacy', component: PrivacyComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'terms', component: TermsComponent },
    { path: 'articles', component: ArticlesComponent },
    { path: 'galleries', component: GalleriesComponent },
    { path: 'videos', component: VideosComponent },
    { path: 'articles/:name', component: ArticleComponent, pathMatch: 'full' },
    { path: 'videos/:name', component: VideoComponent, pathMatch: 'full' },
    { path: 'galleries/:name', component: GalleryComponent, pathMatch: 'full' },
    { path: 'join', component: JoinComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
