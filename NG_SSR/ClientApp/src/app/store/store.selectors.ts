import { createSelector, MemoizedSelector } from '@ngrx/store';
import { RegisterSelectors } from './register';
import { ContactSelectors } from './contact';
import { ContentSelectors } from './content';

export const selectRegisterError: MemoizedSelector<object, string> = createSelector(RegisterSelectors.selectError,
  (registerError: string) => {
    return registerError;
  }
);

export const selectRegisterIsLoading: MemoizedSelector<object, boolean> = createSelector(RegisterSelectors.selectIsLoading,
  (isloading: boolean) => {
    return isloading;
  }
);

export const selectContactError: MemoizedSelector<object, string> = createSelector(ContactSelectors.selectError,
  (registerError: string) => {
    return registerError;
  }
);

export const selectContactIsLoading: MemoizedSelector<object, boolean> = createSelector(ContactSelectors.selectIsLoading,
  (isloading: boolean) => {
    return isloading;
  }
);

export const selectContentError: MemoizedSelector<object, string> = createSelector(ContentSelectors.selectError,
  (registerError: string) => {
    return registerError;
  }
);

export const selectIsLoadingFirstPage: MemoizedSelector<object, boolean> = createSelector(ContentSelectors.selectIsLoadingFirstPage,
  (isloading: boolean) => {
    return isloading;
  }
);
