import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { RegisterModule } from './register/register.module';
import { ContactModule } from './contact/contact.module';
import { ContentModule } from './content/content.module';

@NgModule({
  imports: [
    CommonModule,
    RegisterModule,
    ContactModule,
    ContentModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
  ],
  declarations: []
})
export class RootStoreModule {}
