import * as ContactActions from './contact.actions';
import * as ContactSelectors from './contact.selectors';
import * as ContactState from './contact.state';

export {
  ContactModule
} from './contact.module';

export {
  ContactActions,
  ContactSelectors,
  ContactState
};
