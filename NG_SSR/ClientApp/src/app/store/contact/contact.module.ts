import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ContactEffects } from './contact.effects';
import { ContactReducer } from './contact.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('ContactState', ContactReducer),
    EffectsModule.forFeature([ContactEffects])
  ],
  providers: [ContactEffects]
})
export class ContactModule {}
