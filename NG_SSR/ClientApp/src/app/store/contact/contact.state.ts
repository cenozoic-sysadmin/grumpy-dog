import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Contact } from 'src/app/models/domain.model';

export const featureAdapter: EntityAdapter<Contact> = createEntityAdapter<Contact>({
    selectId: model => model.Message,
    sortComparer: (a: Contact, b: Contact): number =>
    b.Message.toString().localeCompare(a.Message.toString()),
  });

  export interface ContactState extends EntityState<Contact> {
    contact: Contact;
    isLoading?: boolean;
    submitted: boolean;
    referred: boolean;
    error?: any;
  }

  export const initialState: ContactState = featureAdapter.getInitialState(
    {
        contact: new Contact(),
        isLoading: false,
        submitted: false,
        referred: false,
        error: null
    }
  );
