import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { ContactState } from './contact.state';

export const getError = (contactState: ContactState): any => contactState.error;
export const getIsLoading = (contactState: ContactState): boolean => contactState.isLoading;
export const getIsValidated = (contactState: ContactState): boolean =>
    contactState.contact.FirstName != null && contactState.contact.FirstName.length > 0
    && contactState.contact.Email != null && contactState.contact.Email.length > 0
    && contactState.contact.Reason != null && contactState.contact.Reason.length > 0
    && contactState.contact.Message != null && contactState.contact.Message.length > 0;

export const selectContactState: MemoizedSelector<object, ContactState> = createFeatureSelector<ContactState>('ContactState');
export const selectError: MemoizedSelector<object, any> = createSelector(selectContactState, getError);
export const selectIsLoading: MemoizedSelector<object, boolean> = createSelector(selectContactState, getIsLoading);
