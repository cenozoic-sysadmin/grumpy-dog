import { Action } from '@ngrx/store';
import { Owner, Referrals } from '../../models/domain.model';

export enum ActionTypes {
  LOAD_REGISTER = '[Register] Load Register',
  ADD_PET = '[Register] Add Pet',
  REGISTER = '[Register] Register',
  REGISTER_COMPLETE = '[Register] Register Complete',
  UPLOAD_PET_PHOTOS = '[Register] Upload Pet Photos',
  LOAD_REFER = '[Register] Load Refer',
  REFER = '[Register] Refer',
  REFER_COMPLETE = '[Register] Refer Complete',
  LOAD_DONE = '[Register] Load Done'
}

export type Actions =
 LoadRegisterAction | AddPet | RegisterAction | UploadPetPhotos | RegisterCompleteAction
 | LoadReferAction | ReferAction | ReferCompleteAction
 | LoadDoneAction;

export class LoadRegisterAction implements Action {
  readonly type = ActionTypes.LOAD_REGISTER;
}

export class AddPet implements Action {
  readonly type = ActionTypes.ADD_PET;
}

export class RegisterAction implements Action {
  readonly type = ActionTypes.REGISTER;
}

export class RegisterCompleteAction implements Action {
  readonly type = ActionTypes.REGISTER_COMPLETE;
  constructor(public ownerGUID: {item: string}) {}
}

export class UploadPetPhotos implements Action {
  readonly type = ActionTypes.UPLOAD_PET_PHOTOS;
  constructor(public ownerGUID: {item: string}) {}
}

export class LoadReferAction implements Action {
  readonly type = ActionTypes.LOAD_REFER;
  constructor(public ownerGUID: {item: string}) {}
}

export class ReferAction implements Action {
  readonly type = ActionTypes.REFER;
}

export class ReferCompleteAction implements Action {
  readonly type = ActionTypes.REFER_COMPLETE;
  constructor(public success: {item: boolean}) {}
}

export class LoadDoneAction implements Action {
  readonly type = ActionTypes.LOAD_DONE;
}
