import * as RegisterActions from './register.actions';
import * as RegisterSelectors from './register.selectors';
import * as RegisterState from './register.state';

export {
  RegisterModule
} from './register.module';

export {
  RegisterActions,
  RegisterSelectors,
  RegisterState
};
