import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { Owner, Pet, Referrals} from '../../models/domain.model';
import { featureAdapter, RegisterState } from './register.state';

export const getError = (state: RegisterState): any => state.error;
export const getIsLoading = (state: RegisterState): boolean => state.isLoading;
export const getIsRegistered = (state: RegisterState): boolean => state.registered;

export const getIsValidated = (state: RegisterState): boolean =>
    state.owner.Email != null && state.owner.Email.length > 0 && state.owner.Pets.length >= 1;
export const getIsReferrable = (state: RegisterState): boolean => {
    if (state.referrals.Emails === undefined) {
        return false;
    }
    for (let i = 0; i < state.referrals.Emails.length; i++) {
        if (state.referrals.Emails[i] !== undefined) {
            if (state.referrals.Emails[i].length > 0) {
                return true;
            }
        }
    }
    return false;
};


export const selectRegisterState: MemoizedSelector<object, RegisterState> = createFeatureSelector<RegisterState>('RegisterState');

export const selectPetById = (id: number) => createSelector(selectAllPets, (allPets: Pet[]) => {
    if (allPets) {
        return allPets.find(p => p.Id === id);
    } else {
        return null;
    }
});

export const selectPetByIndex = (index: number) => createSelector(selectAllPets, (allPets: Pet[]) => {
    if (allPets) {
        return allPets[index];
    } else {
        return null;
    }
});

export const selectCurrentOwner: MemoizedSelector<object, Owner> = createSelector(selectRegisterState,
    (state: RegisterState): Owner => state.owner
);

export const selectAllPets: MemoizedSelector<object, Pet[]> = createSelector(selectRegisterState,
    (state: RegisterState): Pet[] => state.owner.Pets
);

export const selectReferrals: MemoizedSelector<object, Referrals> = createSelector(selectRegisterState,
    (state: RegisterState): Referrals => state.referrals
);

export const selectIsRegistered: MemoizedSelector<object, boolean> = createSelector(selectRegisterState,
    (state: RegisterState): boolean => state.registered
);

export const selectIsValid: MemoizedSelector<object, boolean> = createSelector(selectRegisterState,
    (state: RegisterState): boolean => state.owner.Email != null && state.owner.Email.length > 0 && state.owner.Pets.length >= 1
);
export const selectError: MemoizedSelector<object, any> = createSelector(selectRegisterState, getError);
export const selectIsLoading: MemoizedSelector<object, boolean> = createSelector(selectRegisterState, getIsLoading);
