import { RegisterState } from './register';
import { ContactState } from './contact';
import { ContentState } from './content';

export interface State {
  RegisterState: RegisterState.RegisterState;
  ContactState: ContactState.ContactState;
  ContentState: ContentState.ContentState;
}
