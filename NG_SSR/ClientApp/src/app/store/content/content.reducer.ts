import { Actions, ActionTypes } from './content.actions';
import { initialState, ContentState } from './content.state';
import { Content } from 'src/app/models/domain.model';

export function ContentReducer(state = initialState, action: Actions): ContentState {
    switch (action.type) {
        case ActionTypes.LOAD_CONTENT:
        case ActionTypes.LOAD_HOME_GALLERY_LIST:
        case ActionTypes.LOAD_HOME_ARTICLE_VIDEO_LIST:
        case ActionTypes.LOAD_CONTENT_LIST:
        {
            return {
                ...state,
                pageNumber: 1,
                isLoading: true,
            };
        }
        case ActionTypes.APPEND_CONTENT_LIST:
        {
            return {
                ...state,
                pageNumber: state.pageNumber + 1,
                isLoading: true,
            };
        }
        case ActionTypes.LOAD_CONTENT_COMPLETE:
        {
            return {
            ...state,
            content: action.payload,
            isLoading: false,
            };
        }
        case ActionTypes.LOAD_CONTENT_LIST_COMPLETE:
        {
            const finalContentList = refreshInfinityMarks(state.contentList, action.payload, action.append);
            return {
                ...state,
                contentList: filter(finalContentList, action.excluding),
                isLoading: false,
            };
        }
        case ActionTypes.LOAD_HOME_ARTICLE_VIDEO_LIST_COMPLETE:
        {
            return {
                ...state,
                pageNumber: 1,
                homePageArticleVideoList: action.payload.splice(0, 10),
                isLoading: false,
            };
        }
        case ActionTypes.LOAD_HOME_GALLERY_LIST_COMPLETE:
        {
            return {
                ...state,
                pageNumber: 1,
                homePageGalleryList: action.payload.splice(0, 6),
                isLoading: false,
            };
        }
        case ActionTypes.TOGGLE_SECTION:
        {
            const sections = state.sections;
            const index = state.sections.indexOf(action.section);
            if (index > -1) {
                sections.splice(index, 1);
            } else {
                sections.push(action.section);
            }
            return {
                ...state,
                pageNumber: 0,
                sections: sections,
                isLoading: true
            };
        }
        case ActionTypes.SET_SECTIONS:
        {
            return {
                ...state,
                pageNumber: 0,
                sections: action.sections,
                isLoading: true
            };
        }
        default: {
            return state;
        }
    }
}

function filter(array: Content[], excluding: Content) {
    if (excluding !== undefined && excluding !== null && excluding.ID > 0) {
        array = array.filter(cont => cont.ID !== excluding.ID);
    }
    return array;
}

function refreshInfinityMarks(oldArray: Content[], newArray: Content[], append: boolean) {
    if (newArray.length === 16)  {
        newArray[8].IsInfinityTrigger = true; // 8th item is infinity trigger, less than 8 = end of content
    }
    if (append) {
        oldArray.map((content, index) => {
            if (content.IsInfinityTrigger) {
                oldArray[index].IsInfinityTrigger = false;
            }
        });
        return oldArray.concat(newArray);
        // TODO: implement merge sort
    }
    return newArray;
}
