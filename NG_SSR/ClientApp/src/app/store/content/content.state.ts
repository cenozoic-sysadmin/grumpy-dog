import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Content } from 'src/app/models/domain.model';

export const featureAdapter: EntityAdapter<Content> = createEntityAdapter<Content>({
  selectId: model => model.ID,
  sortComparer: (a: Content, b: Content): number =>
  b.ID.toString().localeCompare(a.ID.toString()),
});

export interface ContentState extends EntityState<Content> {
    content: Content;
    contentList: Content[];
    homePageArticleVideoList: Content[];
    homePageGalleryList: Content[];
    pageNumber: number;
    sections: string[];
    isLoading?: boolean;
    loaded: boolean;
    error?: any;
}

export const initialState: ContentState = featureAdapter.getInitialState({
    content: new Content(),
    contentList: [],
    homePageArticleVideoList: [],
    homePageGalleryList: [],
    pageNumber: 0,
    sections: [],
    isLoading: true,
    loaded: false,
    error: null
});
