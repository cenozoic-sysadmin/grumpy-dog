import { Action } from '@ngrx/store';
import { Content } from 'src/app/models/domain.model';

export enum ActionTypes {
    LOAD_CONTENT = '[Content] Load Content',
    LOAD_CONTENT_LIST = '[Content] Load Content List',
    APPEND_CONTENT_LIST = '[Content] Append Content List',
    LOAD_HOME_ARTICLE_VIDEO_LIST = '[Content] Load Home Article Video List',
    LOAD_HOME_GALLERY_LIST = '[Content] Load Home Gallery List',

    LOAD_CONTENT_COMPLETE = '[Content] Load Content Complete',
    LOAD_CONTENT_LIST_COMPLETE = '[Content] Load Content List Complete',
    LOAD_HOME_ARTICLE_VIDEO_LIST_COMPLETE = '[Content] Load Home Article Video List Complete',
    LOAD_HOME_GALLERY_LIST_COMPLETE = '[Content] Load Home Gallery List Complete',

    LOAD_META_TAGS = '[Content] Load Meta Tags',
    TOGGLE_SECTION = '[Content] Toggle Section',
    SET_SECTIONS = '[Content] Set Sections'
}

export type Actions =
    LoadContentAction |
    LoadContentListAction |
    AppendContentListAction |
    LoadHomeArticleVideoListAction |
    LoadHomeGalleryListAction |

    LoadContentCompleteAction |
    LoadContentListCompleteAction |
    LoadHomeArticleVideoListCompleteAction |
    LoadHomeGalleryListCompleteAction |

    LoadMetaTagsAction |
    ToggleSectionAction |
    SetSectionsAction;

export class LoadContentAction implements Action {
    readonly type = ActionTypes.LOAD_CONTENT;
    constructor(public contentName: string, public apiName: string) {}
}

export class LoadContentListAction implements Action {
    readonly type = ActionTypes.LOAD_CONTENT_LIST;
    constructor(public excluding: Content, public apiName: string) {}
}

export class AppendContentListAction implements Action {
    readonly type = ActionTypes.APPEND_CONTENT_LIST;
    constructor(public excluding: Content, public apiName: string) {}
}

export class LoadHomeArticleVideoListAction implements Action {
    readonly type = ActionTypes.LOAD_HOME_ARTICLE_VIDEO_LIST;
    constructor() {}
}

export class LoadHomeGalleryListAction implements Action {
    readonly type = ActionTypes.LOAD_HOME_GALLERY_LIST;
    constructor() {}
}

export class LoadContentCompleteAction implements Action {
    readonly type = ActionTypes.LOAD_CONTENT_COMPLETE;
    constructor(public payload: Content) {}
}

export class LoadContentListCompleteAction implements Action {
    readonly type = ActionTypes.LOAD_CONTENT_LIST_COMPLETE;
    constructor(public payload: Content[], public excluding: Content, public append: boolean) {}
}

export class LoadHomeArticleVideoListCompleteAction implements Action {
    readonly type = ActionTypes.LOAD_HOME_ARTICLE_VIDEO_LIST_COMPLETE;
    constructor(public payload: Content[]) {}
}
export class LoadHomeGalleryListCompleteAction implements Action {
    readonly type = ActionTypes.LOAD_HOME_GALLERY_LIST_COMPLETE;
    constructor(public payload: Content[]) {}
}
export class LoadMetaTagsAction implements Action {
    readonly type = ActionTypes.LOAD_META_TAGS;
    constructor(public arg: Content) {}
}

export class ToggleSectionAction implements Action {
    readonly type = ActionTypes.TOGGLE_SECTION;
    public refreshAction: Action;
    constructor(public section: string, loadContentAction: Action) {
        this.refreshAction = loadContentAction;
    }
}

export class SetSectionsAction implements Action {
    readonly type = ActionTypes.SET_SECTIONS;
    public refreshAction: Action;
    constructor(public sections: string[], loadContentAction: Action) {
        this.refreshAction = loadContentAction;
    }
}
