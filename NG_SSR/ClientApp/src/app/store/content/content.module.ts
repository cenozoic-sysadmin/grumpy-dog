import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ContentEffects } from './content.effects';
import { ContentReducer } from './content.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('ContentState', ContentReducer),
    EffectsModule.forFeature([ContentEffects])
  ],
  providers: [ContentEffects]
})
export class ContentModule {}
