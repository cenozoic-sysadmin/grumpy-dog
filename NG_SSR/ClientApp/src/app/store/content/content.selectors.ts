import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { ContentState } from './content.state';
import { Content } from 'src/app/models/domain.model';

export const getError = (contentState: ContentState): any => contentState.error;
export const getIsLoadingFirstPage = (contentState: ContentState): boolean => contentState.isLoading && contentState.pageNumber === 0;
export const getContent = (contentState: ContentState): Content => contentState.content;
export const getContentList = (contentState: ContentState): Content[] => contentState.contentList;
export const getSections = (contentState: ContentState): string[] => contentState.sections;
export const getHomePageGallery = (contentState: ContentState): Content[] => contentState.homePageGalleryList;
export const getHomePageArticleVideoList = (contentState: ContentState): Content[] => contentState.homePageArticleVideoList;

export const getHomePageHeroVideo = (contentState: ContentState): Content => {
    return {
        ... new Content(),
        Type: 'video',
        VideoLink: 'https://droolprodmedia-aueas.streaming.media.azure.net/' +
        '0205e466-ba73-46bd-883a-c0cc5c42379b/Drool_Hero_Video_V1.ism/manifest'
    };
};

export const getHomePagePromoVideo = (contentState: ContentState): Content => {
    return {
        ... new Content(),
        Type: 'video',
        VideoLink: 'https://droolprodmedia-aueas.streaming.media.azure.net/' +
        'e0697cc5-1a18-42b2-a0bd-655fda5629b8/Test_Video_1.ism/manifest'
    };
};


export const selectContentState: MemoizedSelector<object, ContentState> = createFeatureSelector<ContentState>('ContentState');
export const selectError: MemoizedSelector<object, any> = createSelector(selectContentState, getError);
export const selectIsLoadingFirstPage: MemoizedSelector<object, boolean> = createSelector(selectContentState, getIsLoadingFirstPage);

export const selectContent: MemoizedSelector<object, Content> = createSelector(selectContentState, getContent);
export const selectContentList: MemoizedSelector<object, Content[]> = createSelector(selectContentState, getContentList);
export const selectSections: MemoizedSelector<object, string[]> = createSelector(selectContentState, getSections);
export const selectHomePageGalleryList: MemoizedSelector<object, Content[]> = createSelector(selectContentState, getHomePageGallery);
export const selectHomePageArticleVideoList: MemoizedSelector<object, Content[]> = createSelector(selectContentState,
    getHomePageArticleVideoList);

export const selectHomePageHeroVideo: MemoizedSelector<object, Content> = createSelector(selectContentState, getHomePageHeroVideo);
export const selectHomePagePromoVideo: MemoizedSelector<object, Content> = createSelector(selectContentState, getHomePagePromoVideo);
