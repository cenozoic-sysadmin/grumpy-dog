import { Injectable, Inject } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api.service';
import * as ContentActions from './content.actions';
import { RootStoreState } from '../../store';
import { Store } from '@ngrx/store';
import { withLatestFrom } from 'rxjs/operators';


@Injectable()
export class ContentEffects {
    constructor(private api: ApiService, private actions$: Actions, private store$: Store<RootStoreState.State>,
        @Inject('BASE_URL') public blobURL: string
    ) {}

    @Effect()
    LoadContentEffect$: Observable<Action> = this.actions$.pipe(
        ofType<ContentActions.LoadContentAction>(
            ContentActions.ActionTypes.LOAD_CONTENT
        ),
        withLatestFrom(this.store$),
        switchMap(actionstate =>
            this.api.getContent(actionstate[0].contentName, actionstate[0].apiName)
            .pipe(
                map(
                    item => {
                        // replace img path with absolute blob origin
                        item.Body = item.Body.replace('src="/media/', 'src="' + this.blobURL + '/media/');
                        return new ContentActions.LoadContentCompleteAction(item);
                    }
                )
            )
        )
    );

    @Effect()
    LoadContentListEffect$: Observable<Action> = this.actions$.pipe(
        ofType<ContentActions.LoadContentListAction>(
            ContentActions.ActionTypes.LOAD_CONTENT_LIST
        ),
        withLatestFrom(this.store$),
        switchMap(actionstate =>
            this.api.getContentList(actionstate[1].ContentState.pageNumber, actionstate[1].ContentState.sections, actionstate[0].apiName)
            .pipe(
                map(
                    item => new ContentActions.LoadContentListCompleteAction(item, actionstate[0].excluding, false)
                )
            )
        )
    );

    @Effect()
    AppendContentListEffect$: Observable<Action> = this.actions$.pipe(
        ofType<ContentActions.AppendContentListAction>(
            ContentActions.ActionTypes.APPEND_CONTENT_LIST
        ),
        withLatestFrom(this.store$),
        switchMap(actionstate =>
            this.api.getContentList(actionstate[1].ContentState.pageNumber, actionstate[1].ContentState.sections, actionstate[0].apiName)
            .pipe(
                map(
                    item => new ContentActions.LoadContentListCompleteAction(item, actionstate[0].excluding, true)
                )
            )
        )
    );

    @Effect()
    LoadHomeArticleVideoListEffect$: Observable<Action> = this.actions$.pipe(
        ofType<ContentActions.LoadHomeArticleVideoListAction>(
            ContentActions.ActionTypes.LOAD_HOME_ARTICLE_VIDEO_LIST
        ),
        withLatestFrom(this.store$),
        switchMap(actionstate =>
        this.api.getContentList(0, [], 'articlesandvideos')
            .pipe(
                map(
                    item => new ContentActions.LoadHomeArticleVideoListCompleteAction(item)
                )
            )
        )
    );

    @Effect()
    LoadHomeGalleryListEffect$: Observable<Action> = this.actions$.pipe(
        ofType<ContentActions.LoadHomeGalleryListAction>(
            ContentActions.ActionTypes.LOAD_HOME_GALLERY_LIST
        ),
        withLatestFrom(this.store$),
        switchMap(actionstate =>
        this.api.getContentList(0, [], 'galleries')
            .pipe(
                map(
                    item => new ContentActions.LoadHomeGalleryListCompleteAction(item)
                )
            )
        )
    );

    @Effect({dispatch: false})
    MetaTagsLoadEffect$: Observable<Action> = this.actions$.pipe(
        ofType<ContentActions.LoadMetaTagsAction>(
            ContentActions.ActionTypes.LOAD_META_TAGS
        ),
        withLatestFrom(this.store$),
        switchMap((actionstate) => {
            this.api.populateMetaTags(actionstate[0].arg);
            return new Observable<Action>();
        })
    );

    @Effect()
    ToggleSectionEffect$: Observable<Action> = this.actions$.pipe(
        ofType<ContentActions.ToggleSectionAction>(
            ContentActions.ActionTypes.TOGGLE_SECTION
        ),
        withLatestFrom(this.store$),
        map(
            actionstate => actionstate[0].refreshAction
        )
    );

    @Effect()
    SetSectionEffect$: Observable<Action> = this.actions$.pipe(
        ofType<ContentActions.SetSectionsAction>(
            ContentActions.ActionTypes.SET_SECTIONS
        ),
        withLatestFrom(this.store$),
        map(
            actionstate => actionstate[0].refreshAction
        )
    );

}
