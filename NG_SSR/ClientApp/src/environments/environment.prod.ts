export const environment = {
    production: true,
    origin: 'https://www.drool.pet',
    cmsURL: 'https://cms.drool.pet',
    singrURL: 'https://www.singr.pet'
};

/* export const environment = {
    production: false,
    origin: 'http://devssr.drool.pet',
    cmsURL: 'http://devcms.drool.pet',
    singrURL: 'http://dev.singr.pet'
}; */

/* export const environment = {
    production: false,
    origin: 'http://localhost:5000',
    cmsURL: 'http://localhost:8619',
    singrURL: 'http://dev.singr.pet'
}; */
